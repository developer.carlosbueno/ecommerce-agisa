import React, {
  ReactElement,
  createContext,
  useContext,
  useState,
  useEffect,
} from 'react'

// Services
import { Cart } from '@/models/Cart'
import { Product } from '@/models/Product'
import { toast } from 'react-hot-toast'

const initialState: InitialStateProps = {
  cart: [],
  addToCart: () => { },
  removeFromCart: () => { },
}

const FavoriteContext = createContext(initialState)

export interface FavoriteProviderProps {
  children: ReactElement
}

export const FavoriteProvider: React.FC<FavoriteProviderProps> = ({ children }) => {
  const [cart, setCart] = useState<Cart[]>(initialState.cart)

  useEffect(() => {
    getInitialState()
  }, [])

  const getInitialState = async () => {
    try {
    } catch (error) { }
  }

  const addToCart = (product: Product, qty: number = 1, size: string) => {
    console.log('CART', cart)
    console.log('info', qty, size)
    let exist = cart.find(
      (i) => i.product._id === product._id && i.size === size
    )

    if (exist) {
      setCart((prev) =>
        prev.map((i) =>
          i.product.name === product.name && i.size === size
            ? { ...i, qty: i.qty + 1 }
            : i
        )
      )
      toast.success('Producto añadido al carrito!')
    } else {
      toast.success('Producto añadido al carrito!')
      setCart((prev) => [{ product, qty, size }, ...prev])
    }
  }

  const removeFromCart = (product: Product, size: string) => {
    let copy: any[] = []

    cart.map((i) => {
      if (i.product._id === product._id && i.size === size) {
        if (i.qty > 1) copy.push({ ...i, qty: i.qty - 1 })
      } else copy.push(i)
    })

    setCart(copy)

    toast.success('Producto eliminado del carrito!')
  }

  return (
    <FavoriteContext.Provider value= {{ cart, addToCart, removeFromCart }
}>
  { children }
  < /FavoriteContext.Provider>
  )
}

export const useCartState = () => useContext(FavoriteContext)

interface InitialStateProps {
  cart: Cart[]
  addToCart: (product: Product, qty: number, size: string) => void
  removeFromCart: (product: Product, size: string) => void
}
