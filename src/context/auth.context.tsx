import React, {
  ReactElement,
  createContext,
  useContext,
  useState,
  useEffect,
} from 'react'

// Services
import { Product } from '@/models/Product'
import { toast } from 'react-hot-toast'
import { Auth, CreateAuthDtop, LoginDto } from '@/models/Auth'
import { loginUser, myOrders, signup } from '@/services/auth.service'
import { getLocalStore, removetLocalStore } from '@/utils/getLocalStore'
import { useNavigate } from 'react-router-dom'

const initialState: InitialStateProps = {
  auth: null,
  authOrders: [],
  login: () => {},
  getMyOrders: () => {},
  register: () => {},
  logout: () => {},
}

const AuthContext = createContext(initialState)

export interface AuthProviderProps {
  children: ReactElement
}

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const navigate = useNavigate()
  const [auth, setAuth] = useState<Auth | null>(
    getLocalStore('auth') || initialState.auth
  )
  const [authOrders, setAuthOrders] = useState<any[]>([])

  useEffect(() => {
    getInitialState()
  }, [])

  const getInitialState = async () => {
    try {
    } catch (error) {}
  }

  const login = async (body: LoginDto) => {
    try {
      const auth = await loginUser(body)
      setAuth(auth)
      console.log('AUTH LOGIN', auth)
    } catch (error: any) {
      console.log('error login', error.message)
      throw new Error(error.message)
    }
  }
  const register = async (body: CreateAuthDtop) => {
    try {
      const auth = await signup(body)
      setAuth(auth)
      console.log('AUTH REGISTER', auth)
    } catch (error: any) {
      console.log('error register', error.message)
      throw new Error(error.message)
    }
  }

  const getMyOrders = async () => {
    try {
      const orders = await myOrders(auth?._id!)
      setAuthOrders(orders)
    } catch (error: any) {
      toast.error(error.message)
    }
  }

  const logout = () => {
    removetLocalStore('auth')
    setAuth(null)
    toast.success('Has salido de tu cuenta!')
    navigate('/home')
  }

  return (
    <AuthContext.Provider
      value={{ auth, login, authOrders, register, logout, getMyOrders }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export const useAuthState = () => useContext(AuthContext)

interface InitialStateProps {
  auth: Auth | null
  authOrders: any[]
  login: (body: LoginDto) => void
  register: (body: CreateAuthDtop) => void
  getMyOrders: () => void
  logout: () => void
}
