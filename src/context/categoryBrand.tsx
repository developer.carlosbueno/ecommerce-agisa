import React, {
  ReactElement,
  createContext,
  useContext,
  useState,
  useEffect,
} from 'react'
import { ICategory, IBrand } from '@services/categoryBrand.service'

// Services
import { getCategories, getBrands } from '@/services/categoryBrand.service'

const initialState: InitialStateProps = {
  categories: [],
  category: { name: '', _id: '', href: '/' },
  brands: [],
  brand: { name: '', _id: '' },
}

const CategoryBrandContext = createContext(initialState)

export interface CategoryBrandProviderProps {
  children: ReactElement
}

export const CategoryBrandProvider: React.FC<CategoryBrandProviderProps> = ({
  children,
}) => {
  const [categories, setCategories] = useState<ICategory[]>(
    initialState.categories
  )
  const [category, setCategory] = useState<ICategory>(initialState.category)
  const [brands, setBrands] = useState<IBrand[]>(initialState.brands)
  const [brand, setBrand] = useState<IBrand>(initialState.brand)

  useEffect(() => {
    getInitialState()
  }, [])

  const getInitialState = async () => {
    try {
      const categoriesData = await getCategories()
      const brandsData = await getBrands()
      setCategories(categoriesData)
      setBrands(brandsData)
    } catch (error) {}
  }

  return (
    <CategoryBrandContext.Provider
      value={{ categories, category, brands, brand }}
    >
      {children}
    </CategoryBrandContext.Provider>
  )
}

export const useCategoryBrandState = () => useContext(CategoryBrandContext)

interface InitialStateProps {
  categories: ICategory[]
  category: ICategory
  brands: IBrand[]
  brand: IBrand
}
