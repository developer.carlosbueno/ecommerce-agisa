import { SERVER_URL_IMAGE } from '@/config/config'
import { useCartState } from '@/context/cart'
import { Product } from '@/models/Product'
import { ButtonPrimary } from '@components/shared/Button/ButtonPrimary'
import ButtonSecondary from '@components/shared/Button/ButtonSecondary'
import { DEMO_VARIANTS } from '@data/data'
import { ArrowsPointingOutIcon } from '@heroicons/react/24/outline'
import React, { FC, useEffect } from 'react'
import { Link } from 'react-router-dom'
import BagIcon from '../BagIcon'
import LikeButton from '../LikeButton'
import ModalQuickView from '../ModalQuickView'
import Prices from '../Prices'
import ProductStatus from '../ProductStatus'

const status = 'New in'

export interface ProductCardProps {
  className?: string
  data: Product
  isLiked?: boolean
}

const ProductCard: FC<ProductCardProps> = ({
  className = '',
  data,
  isLiked,
}) => {
  const { addToCart } = useCartState()
  const { images, name, price, sizes } = data

  const [showModalQuickView, setShowModalQuickView] = React.useState(false)

  // sizes: ["XS", "S", "M", "L", "XL"],

  const notifyAddTocart = ({
    size,
    product,
    qty,
  }: {
    size: string
    product: Product
    qty: number
  }) => {
    addToCart(product, qty, size)
  }

  // const renderProductCartOnNotify = ({ size }: { size?: string }) => {
  //   return (
  //     <div className="flex ">
  //       <div className="h-24 w-20 flex-shrink-0 overflow-hidden rounded-xl bg-slate-100">
  //         <img
  //           src={SERVER_URL_IMAGE + images[0]}
  //           alt={name}
  //           className="h-full w-full object-cover object-center"
  //         />
  //       </div>

  //       <div className="ml-4 flex flex-1 flex-col">
  //         <div>
  //           <div className="flex justify-between ">
  //             <div>
  //               <h3 className="text-base font-medium ">{name}</h3>
  //               <p className="mt-1 text-sm text-slate-500 dark:text-slate-400">
  //                 <span>
  //                   {variants ? variants[variantActive].name : `Natural`}
  //                 </span>
  //                 <span className="mx-2 border-l border-slate-200 dark:border-slate-700 h-4"></span>
  //                 <span>{size || "XL"}</span>
  //               </p>
  //             </div>
  //             <Prices price={price} className="mt-0.5" />
  //           </div>
  //         </div>
  //         <div className="flex flex-1 items-end justify-between text-sm">
  //           <p className="text-gray-500 dark:text-slate-400">Qty 1</p>

  //           <div className="flex">
  //             <Link
  //               to={"/cart"}
  //               className="font-medium text-primary-6000 dark:text-primary-500 "
  //             >
  //               View cart
  //             </Link>
  //           </div>
  //         </div>
  //       </div>
  //     </div>
  //   );
  // };

  const renderGroupButtons = () => {
    return (
      <div className='absolute bottom-0 group-hover:bottom-4 inset-x-1 flex justify-center opacity-0 invisible group-hover:opacity-100 group-hover:visible transition-all'>
        <ButtonPrimary
          className='shadow-lg'
          fontSize='text-xs'
          sizeClass='py-2 px-4'
          onClick={() => notifyAddTocart({ size: 'XL', product: data, qty: 1 })}
        >
          <BagIcon className='w-3.5 h-3.5 mb-0.5' />
          <span className='ml-1'>Add to bag</span>
        </ButtonPrimary>
        <ButtonSecondary
          className='ml-1.5 bg-white hover:!bg-gray-100 hover:text-slate-900 transition-colors shadow-lg'
          fontSize='text-xs'
          sizeClass='py-2 px-4'
          onClick={() => setShowModalQuickView(true)}
        >
          <ArrowsPointingOutIcon className='w-3.5 h-3.5' />
          <span className='ml-1'>Quick view</span>
        </ButtonSecondary>
      </div>
    )
  }

  const renderSizeList = () => {
    if (!sizes || !sizes.length) {
      return null
    }

    return (
      <div className='absolute bottom-0 inset-x-1 space-x-1.5 flex justify-center opacity-0 invisible group-hover:bottom-4 group-hover:opacity-100 group-hover:visible transition-all'>
        {sizes.map((size, index) => {
          return (
            <div
              key={index}
              className='nc-shadow-lg w-10 h-10 rounded-xl bg-white hover:bg-slate-900 hover:text-white transition-colors cursor-pointer flex items-center justify-center uppercase font-semibold tracking-tight text-sm text-slate-900'
              onClick={() =>
                notifyAddTocart({ size: size.name, product: data, qty: 1 })
              }
            >
              {size.name}
            </div>
          )
        })}
      </div>
    )
  }

  return (
    <>
      <div
        className={`nc-ProductCard relative flex flex-col bg-transparent  ${className}`}
        data-nc-id='ProductCard'
      >
        <Link to={`/product/${data._id}`} className='absolute inset-0'></Link>

        <div className='relative flex-shrink-0 bg-slate-50 dark:bg-slate-300  overflow-hidden z-1 group shadow-md'>
          <Link to={`/product/${data._id}`} className='block'>
            <img
              // containerClassName='flex aspect-w-11 aspect-h-12 w-full h-0'
              src={SERVER_URL_IMAGE + images[0]}
              className='object-cover w-full h-full drop-shadow-xl'
            />
          </Link>

          <ProductStatus status={status} />

          <LikeButton liked={isLiked} className='absolute top-3 right-3' />

          {sizes.length ? renderSizeList() : renderGroupButtons()}
        </div>

        <div className='space-y-4 px-2.5 pt-5 pb-2.5'>
          <div className='flex items-center justify-between'>
            <h2
              className={`nc-ProductCard__title text-base font-semibold transition-colors dark:text-neutral-300`}
            >
              {name}
            </h2>
            <p className={`text-sm text-slate-500 dark:text-slate-400 mt-1 `}>
              <Prices price={price} />
            </p>
          </div>

          <div className='flex justify-end items-end '>
            {/* <div className="flex items-center mb-0.5">
              <StarIcon className="w-5 h-5 pb-[1px] text-amber-400" />
              <span className="text-sm ml-1 text-slate-500 dark:text-slate-400">
                {(Math.random() * 1 + 4).toFixed(1)} (
                {Math.floor(Math.random() * 70 + 20)} reviews)
              </span>
            </div> */}
          </div>
        </div>
      </div>

      {/* QUICKVIEW */}
      <ModalQuickView
        show={showModalQuickView}
        onCloseModalQuickView={() => setShowModalQuickView(false)}
      />
    </>
  )
}

export default ProductCard
