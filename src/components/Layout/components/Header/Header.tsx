import React, { FC, useState } from 'react'
import Logo from '@components/shared/Logo'
// import { XMarkIcon } from '@heroicons/react/24/outline'
import { useNavigate } from 'react-router-dom'
import { MenuBar } from '@/components/shared'
import Navigation from '@/components/Navigation/Navigation'
import { XMarkIcon } from '@heroicons/react/24/solid'
import AvatarDropdown from './components/AvatarDropdown'
import CartDropdown from './components/CartDropwdown'
import { Link } from 'react-router-dom'

export interface MainNav2LoggedProps {}

const MainNav2Logged: FC<MainNav2LoggedProps> = () => {
  const inputRef = React.createRef<HTMLInputElement>()
  const [showSearchForm, setShowSearchForm] = useState(false)
  const navigate = useNavigate()

  const renderMagnifyingGlassIcon = () => {
    return (
      <svg
        width={22}
        height={22}
        viewBox='0 0 24 24'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z'
          stroke='currentColor'
          strokeWidth='1.5'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M22 22L20 20'
          stroke='currentColor'
          strokeWidth='1.5'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </svg>
    )
  }

  const renderSearchForm = () => {
    return (
      <form
        onSubmit={(e) => {
          e.preventDefault()
          navigate('/page-search')
        }}
        className='flex-1 py-2 text-slate-900 dark:text-slate-100'
      >
        <div className='bg-slate-50 dark:bg-slate-800 flex items-center space-x-1.5 px-5 h-full rounded'>
          {renderMagnifyingGlassIcon()}
          <input
            ref={inputRef}
            type='text'
            placeholder='Type and press enter'
            className='border-none bg-transparent focus:outline-none focus:ring-0 w-full text-base'
            autoFocus
          />
          <button type='button' onClick={() => setShowSearchForm(false)}>
            <XMarkIcon className='w-5 h-5' />
          </button>
        </div>
        <input type='submit' hidden value='' />
      </form>
    )
  }

  const renderContent = () => {
    return (
      <div className='h-20 flex justify-between z-50'>
        <div className='flex items-center lg:hidden flex-1'>
          <MenuBar />
        </div>

        <div className='lg:flex-1 flex items-center'>
          <Link
            to='/home'
            className='ttnc-logo  text-slate-800 flex items-center hover:scale-105 transition-all'
          >
            {/* <Logo className='flex-shrink-0' /> */}
            <img
              src='https://cdn-icons-png.flaticon.com/512/874/874928.png'
              className='object-contain h-[4.5vh] mr-1'
              alt=''
            />
            <h1 className='text-2xl font-semibold underline decoration-lime-400 dark:text-slate-200'>
              Agisa Fashion
            </h1>
          </Link>
        </div>

        <div className='flex-[2] hidden lg:flex justify-center mx-4'>
          {showSearchForm ? renderSearchForm() : <Navigation />}
        </div>

        <div className='flex-1 flex items-center justify-end text-slate-700 dark:text-slate-100'>
          {!showSearchForm && (
            <button
              className='hidden lg:flex w-10 h-10 sm:w-12 sm:h-12 rounded-full text-slate-700 dark:text-slate-300 hover:bg-lime-100 dark:hover:bg-slate-800 focus:outline-none items-center justify-center'
              onClick={() => setShowSearchForm(!showSearchForm)}
            >
              {renderMagnifyingGlassIcon()}
            </button>
          )}
          <AvatarDropdown />
          <CartDropdown />
        </div>
      </div>
    )
  }

  return (
    <div className='nc-MainNav2Logged relative z-10 bg-white dark:bg-slate-900 border-b border-slate-100 dark:border-slate-700'>
      <div className='container '>{renderContent()}</div>
    </div>
  )
}

export default MainNav2Logged
