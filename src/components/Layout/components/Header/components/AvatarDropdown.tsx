import Avatar from '@/components/shared/Avatar'
import { Popover, Transition } from '@headlessui/react'
import { avatarImgs } from '@components/Contains/fakeData'
import { Fragment } from 'react'
import { Link } from 'react-router-dom'
import { SwitchDarkMode } from '@/components/shared'
import { useAuthState } from '@/context/auth.context'
import Profile from './icons/Profile'
import Checkout from './icons/Checkout'
import Lightbulb from './icons/Lightbulb'
import LifeBoat from './icons/LifeBoat'
import Logout from './icons/Logout'

export default function AvatarDropdown() {
  const { auth, logout } = useAuthState()

  return (
    <div className='AvatarDropdown '>
      <Popover className='relative'>
        {({ open, close }) => (
          <>
            <Popover.Button
              className={`w-10 h-10 sm:w-12 sm:h-12 rounded-full text-slate-700 dark:text-slate-300 hover:bg-lime-100 dark:hover:bg-slate-800 focus:outline-none flex items-center justify-center`}
            >
              <svg
                className=' w-6 h-6'
                viewBox='0 0 24 24'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
              >
                <path
                  d='M12 12C14.7614 12 17 9.76142 17 7C17 4.23858 14.7614 2 12 2C9.23858 2 7 4.23858 7 7C7 9.76142 9.23858 12 12 12Z'
                  stroke='currentColor'
                  strokeWidth='1.5'
                  strokeLinecap='round'
                  strokeLinejoin='round'
                />
                <path
                  d='M20.5899 22C20.5899 18.13 16.7399 15 11.9999 15C7.25991 15 3.40991 18.13 3.40991 22'
                  stroke='currentColor'
                  strokeWidth='1.5'
                  strokeLinecap='round'
                  strokeLinejoin='round'
                />
              </svg>
            </Popover.Button>
            <Transition
              as={Fragment}
              enter='transition ease-out duration-200'
              enterFrom='opacity-0 translate-y-1'
              enterTo='opacity-100 translate-y-0'
              leave='transition ease-in duration-150'
              leaveFrom='opacity-100 translate-y-0'
              leaveTo='opacity-0 translate-y-1'
            >
              <Popover.Panel className='absolute z-10 w-screen max-w-[260px] px-4 mt-3.5 -right-10 sm:right-0 sm:px-0'>
                <div className='overflow-hidden rounded-3xl shadow-lg ring-1 ring-black ring-opacity-5'>
                  <div className='relative grid grid-cols-1 gap-6 bg-white dark:bg-neutral-800 py-7 px-6'>
                    {auth && (
                      <>
                        <div className='flex items-center space-x-3'>
                          <div className='flex-grow'>
                            <h4 className='font-semibold'>{auth?.fullname}</h4>
                          </div>
                        </div>

                        <div className='w-full border-b border-neutral-200 dark:border-neutral-700' />

                        {/* ------------------ 1 --------------------- */}
                        <Link
                          to={'/account'}
                          className='flex items-center p-2 -m-3 transition duration-150 ease-in-out rounded-lg hover:bg-lime-100 dark:hover:bg-neutral-700 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50'
                          onClick={() => close()}
                        >
                          <div className='flex items-center justify-center flex-shrink-0 text-neutral-500 dark:text-neutral-300'>
                            <Profile />
                          </div>
                          <div className='ml-4'>
                            <p className='text-sm font-medium '>{'Cuenta'}</p>
                          </div>
                        </Link>

                        {/* ------------------ 2 --------------------- */}
                        <Link
                          to={'/account-my-order'}
                          className='flex items-center p-2 -m-3 transition duration-150 ease-in-out rounded-lg hover:bg-lime-100 dark:hover:bg-neutral-700 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50'
                          onClick={() => close()}
                        >
                          <div className='flex items-center justify-center flex-shrink-0 text-neutral-500 dark:text-neutral-300'>
                            <Checkout />
                          </div>
                          <div className='ml-4'>
                            <p className='text-sm font-medium '>{'Ordenes'}</p>
                          </div>
                        </Link>

                        {/* ------------------ 2 --------------------- */}
                        <Link
                          to={'/account-savelists'}
                          className='flex items-center p-2 -m-3 transition duration-150 ease-in-out rounded-lg hover:bg-lime-100 dark:hover:bg-neutral-700 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50'
                          onClick={() => close()}
                        >
                          <div className='flex items-center justify-center flex-shrink-0 text-neutral-500 dark:text-neutral-300'>
                            <svg
                              width='24'
                              height='24'
                              viewBox='0 0 24 24'
                              fill='none'
                            >
                              <path
                                d='M12.62 20.81C12.28 20.93 11.72 20.93 11.38 20.81C8.48 19.82 2 15.69 2 8.68998C2 5.59998 4.49 3.09998 7.56 3.09998C9.38 3.09998 10.99 3.97998 12 5.33998C13.01 3.97998 14.63 3.09998 16.44 3.09998C19.51 3.09998 22 5.59998 22 8.68998C22 15.69 15.52 19.82 12.62 20.81Z'
                                stroke='currentColor'
                                strokeWidth='1.5'
                                strokeLinecap='round'
                                strokeLinejoin='round'
                              />
                            </svg>
                          </div>
                          <div className='ml-4'>
                            <p className='text-sm font-medium '>
                              {'Favoritos'}
                            </p>
                          </div>
                        </Link>

                        <div className='w-full border-b border-neutral-200 dark:border-neutral-700' />
                      </>
                    )}

                    {!auth && (
                      <>
                        <Link
                          to={'/login'}
                          className='flex items-center p-2 -m-3 transition duration-150 ease-in-out rounded-lg hover:bg-lime-200 dark:hover:bg-neutral-700 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50'
                          onClick={() => close()}
                        >
                          <div className='flex items-center justify-center flex-shrink-0 text-neutral-500 dark:text-neutral-300'>
                            <Profile />
                          </div>
                          <div className='ml-4'>
                            <p className='text-sm font-medium '>
                              Iniciar seccion
                            </p>
                          </div>
                        </Link>

                        <div className='w-full border-b border-neutral-200 dark:border-neutral-700' />
                      </>
                    )}

                    {/* ------------------ 2 --------------------- */}
                    <Link
                      to={'/#'}
                      className='flex items-center p-2 -m-3 transition duration-150 ease-in-out rounded-lg hover:bg-lime-200 dark:hover:bg-neutral-700 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50'
                      onClick={() => close()}
                    >
                      <div className='flex items-center justify-center flex-shrink-0 text-neutral-500 dark:text-neutral-300'>
                        <LifeBoat />
                      </div>
                      <div className='ml-4'>
                        <p className='text-sm font-medium '>{'Ayuda'}</p>
                      </div>
                    </Link>

                    {/* ------------------ 2 --------------------- */}
                    <div className='flex items-center justify-between p-2 -m-3 transition duration-150 ease-in-out rounded-lg hover:bg-lime-100 dark:hover:bg-neutral-700 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50'>
                      <div className='flex items-center'>
                        <div className='flex items-center justify-center flex-shrink-0 text-neutral-500 dark:text-neutral-300'>
                          <Lightbulb />
                        </div>
                        <div className='ml-4'>
                          <p className='text-sm font-medium '>
                            {'Modo Oscuro'}
                          </p>
                        </div>
                      </div>
                      <SwitchDarkMode />
                    </div>

                    {/* ------------------ 2 --------------------- */}
                    <div
                      className='flex items-center p-2 -m-3 transition duration-150 ease-in-out rounded-lg hover:bg-lime-100 dark:hover:bg-neutral-700 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50'
                      onClick={() => logout()}
                    >
                      <div className='flex items-center justify-center flex-shrink-0 text-neutral-500 dark:text-neutral-300'>
                        <Logout />
                      </div>
                      <div className='ml-4'>
                        <p className='text-sm font-medium '>{'Salir'}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    </div>
  )
}
