import Button, { ButtonProps } from '@components/shared/Button/Button'
import React from 'react'

export interface ButtonPrimaryProps extends ButtonProps {}

const ButtonPrimary: React.FC<ButtonPrimaryProps> = ({
  className = '',
  ...args
}) => {
  return (
    <Button
      className={`ttnc-ButtonPrimary disabled:bg-opacity-90 bg-lime-300 dark:bg-slate-100 hover:bg-lime-500 hover:text-white dark:hover:text-slate-700 dark:hover:bg-slate-50 text-slate-900 dark:text-slate-800 shadow-xl ${className}`}
      {...args}
    />
  )
}

export default ButtonPrimary
