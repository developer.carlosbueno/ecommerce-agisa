import React, { FC, InputHTMLAttributes } from 'react'

export interface RadioProps extends InputHTMLAttributes<HTMLInputElement> {
  className?: string
  name: string
  id: string
  defaultChecked?: boolean
  sizeClassName?: string
  label?: string
}

const Radio = React.forwardRef<HTMLInputElement, RadioProps>(
  (
    {
      className = '',
      name,
      id,
      label,
      sizeClassName = 'w-6 h-6',
      defaultChecked,
      ...args
    },
    ref
  ) => {
    return (
      <div className={`flex items-center text-sm sm:text-base ${className}`}>
        <input
          id={id}
          ref={ref}
          name={name}
          type='radio'
          className={`focus:ring-action-primary text-primary-500 rounded-full border-slate-400 hover:border-slate-700 bg-transparent dark:border-slate-700 dark:hover:border-slate-500 dark:checked:bg-primary-500 focus:ring-primary-500 ${sizeClassName}`}
          value={id}
          {...args}
        />
        {label && (
          <label
            htmlFor={id}
            className='pl-2.5 sm:pl-3 block text-slate-900 dark:text-slate-100 select-none'
            dangerouslySetInnerHTML={{ __html: label }}
          ></label>
        )}
      </div>
    )
  }
)

export default Radio
