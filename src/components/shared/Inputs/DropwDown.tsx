import React, { SelectHTMLAttributes } from 'react'

export interface DropDownProps extends SelectHTMLAttributes<HTMLSelectElement> {
  sizeClass?: string
  fontClass?: string
  rounded?: string
  options: any[]
  optionLabel?: string
  optionValue?: string
}

const DropDown = React.forwardRef<HTMLSelectElement, DropDownProps>(
  ({
    className = '',
    sizeClass = 'h-11 px-4 py-3',
    fontClass = 'text-sm font-normal',
    rounded = 'rounded-sm',
    children,
    options,
    optionLabel,
    optionValue,
    ...args
  }) => {
    return (
      <select
        {...args}
        className={`block w-full border border-zinc-200 focus:border-primary-300 focus:ring  focus:ring-opacity-50 bg-white dark:border-neutral-500 dark:border dark:focus:ring-primary-6000 dark:focus:ring-opacity-25 dark:bg-transparent disabled:bg-neutral-200 dark:disabled:bg-neutral-800 ${rounded} ${fontClass} ${sizeClass} ${className}`}
      >
        {options.map((option) => (
          <option
            className={'dark:text-black'}
            value={optionValue ? option[optionValue] : option}
          >
            {optionLabel ? option[optionLabel] : option}
          </option>
        ))}
      </select>
    )
  }
)

export default DropDown
