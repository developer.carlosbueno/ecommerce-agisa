import React from 'react'

export interface IMessageProps {
  message: string
  className?: string
  type: 'error' | 'success'
}

const Message: React.FC<IMessageProps> = ({ message, type, className }) => {
  return (
    <div
      className={`p-4 rounded-md ${
        type === 'error'
          ? 'bg-red-400 text-red-900'
          : 'bg-lime-400 text-lime-900'
      } ${className}`}
    >
      {message}
    </div>
  )
}

export default Message
