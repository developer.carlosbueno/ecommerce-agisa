import { FC } from 'react'
import { SocialType } from '../SocialShared'

export interface SocialsListProps {
  className?: string
  itemClass?: string
  socials?: SocialType[]
}

const socialsDemo: SocialType[] = [
  {
    name: 'Facebook',
    icon: 'https://ciseco-reactjs.vercel.app/static/media/facebook.8291c7f7c187e8f09292cced2ed0278d.svg',
    href: '#',
  },
  {
    name: 'Twitter',
    icon: 'https://ciseco-reactjs.vercel.app/static/media/twitter.f56ce1bc9eb5120250ac80ed561cf82f.svg',
    href: '#',
  },
  {
    name: 'Youtube',
    icon: 'https://ciseco-reactjs.vercel.app/static/media/youtube.bb2387598b5621f3a2e92ab928da4fe0.svg',
    href: '#',
  },
  {
    name: 'Telegram',
    icon: 'https://ciseco-reactjs.vercel.app/static/media/telegram.5acad1587076bc12320cadff0f4aa3f3.svg',
    href: '#',
  },
]

const SocialsList: FC<SocialsListProps> = ({
  className = '',
  itemClass = 'block w-6 h-6',
  socials = socialsDemo,
}) => {
  return (
    <nav
      className={`nc-SocialsList flex space-x-2.5 text-2xl text-neutral-6000 dark:text-neutral-300 ${className}`}
      data-nc-id='SocialsList'
    >
      {socials.map((item, i) => (
        <a
          key={i}
          className={`${itemClass}`}
          href={item.href}
          target='_blank'
          rel='noopener noreferrer'
          title={item.name}
        >
          <img src={item.icon} alt='' />
        </a>
      ))}
    </nav>
  )
}

export default SocialsList
