import React, { FC } from 'react'
import ButtonPrimary from '@components/shared/Button/ButtonPrimary/ButtonPrimary'

export interface SectionHero3Props {
  className?: string
}

const SectionHero3: FC<SectionHero3Props> = ({ className = '' }) => {
  return (
    <div className={`overflow-hidden container xl:px-0 relative ${className}`}>
      <div className=' relative pt-8 lg:pt-0 lg:absolute z-[5] inset-x-0 top-[10%] sm:top-[20%]  container'>
        <div className='flex flex-col items-start max-w-lg xl:max-w-2xl space-y-5 xl:space-y-8 '>
          <span className='sm:text-lg md:text-xl font-semibold text-zinc-800'>
            Solo nuevas y premium 🔥
          </span>
          <h2 className='font-bold text-zinc-800 text-3xl sm:text-4xl md:text-5xl xl:text-6xl 2xl:text-7xl !leading-[115%] '>
            Cálidad y precio.
          </h2>
          <div className='sm:pt-4'>
            <ButtonPrimary
              sizeClass='px-6 py-3 lg:px-8 lg:py-4'
              fontSize='text-sm sm:text-base lg:text-lg font-medium'
            >
              Empieza a buscar
            </ButtonPrimary>
          </div>
        </div>
      </div>

      <div className='w-full px-10 relative h-[68vh] z-[1] lg:aspect-w-16 lg:aspect-h-8 2xl:aspect-h-7'>
        <div className='w-full '>
          <div className='mt-5 lg:mt-0 lg:absolute right-0 bottom-0 top-0 w-full max-w-xl lg:max-w-2xl xl:max-w-3xl 2xl:max-w-4xl ml-auto'>
            <img
              className='w-full sm:h-full object-contain object-right-bottom '
              src='https://ciseco-reactjs.vercel.app/static/media/hero-2-right-1.3e7bed1e0c42a30e5b95.png'
              alt=''
            />
          </div>
        </div>
      </div>

      {/* BG */}
      {/* bg-[#F7F0EA] */}
      <div className='absolute inset-0 xl:inset-y-0 xl:-inset-x-[9.3vw]  bg-amber-100 rounded-sm overflow-hidden z-0'>
        <img
          className='absolute w-full h-full object-contain'
          src='https://ciseco-reactjs.vercel.app/static/media/Moon.bf27dc577d1acccaba48430d353dbbe0.svg'
          alt='hero'
        />
      </div>
    </div>
  )
}

export default SectionHero3
