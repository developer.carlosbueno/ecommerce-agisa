import React, { FC } from 'react'
import NcImage from '@components/shared/NcImage'
import Badge from '@components/shared/Badge'

export interface SectionHowItWorkProps {
  className?: string
  data?: (typeof DEMO_DATA)[0][]
}

const DEMO_DATA = [
  {
    id: 1,
    img: 'https://ciseco-reactjs.vercel.app/static/media/HIW1img.0a618bef4b830b5c682b.png',
    imgDark:
      'https://ciseco-reactjs.vercel.app/static/media/HIW1img.0a618bef4b830b5c682b.png',
    title: 'Filtra y Descubre',
    desc: 'Encuentra tu producto deseado de forma sencilla',
  },
  {
    id: 2,
    img: 'https://ciseco-reactjs.vercel.app/static/media/HIW2img.3d1e1bf2693a9272e96a.png',
    imgDark:
      'https://ciseco-reactjs.vercel.app/static/media/HIW2img.3d1e1bf2693a9272e96a.png',
    title: 'Agrega al carrito',
    desc: 'Seleccione fácilmente los artículos correctos y agréguelos al carrito',
  },
  {
    id: 3,
    img: 'https://ciseco-reactjs.vercel.app/static/media/HIW3img.8b344c9a046e6b85a9ab.png',
    imgDark:
      'https://ciseco-reactjs.vercel.app/static/media/HIW3img.8b344c9a046e6b85a9ab.png',
    title: 'Envio Rapido',
    desc: 'Recibe tu producto hasta en 24 horas',
  },
  {
    id: 4,
    img: 'https://ciseco-reactjs.vercel.app/static/media/HIW4img.c6a12d19e09f27d32678.png',
    imgDark:
      'https://ciseco-reactjs.vercel.app/static/media/HIW4img.c6a12d19e09f27d32678.png',
    title: 'Disfruta el producto',
    desc: 'Diviértete y disfruta de tus productos de calidad 5 estrellas',
  },
]

const SectionHowItWork: FC<SectionHowItWorkProps> = ({
  className = '',
  data = DEMO_DATA,
}) => {
  return (
    <div
      className={`nc-SectionHowItWork ${className}`}
      data-nc-id='SectionHowItWork'
    >
      <div className='relative grid sm:grid-cols-2 lg:grid-cols-4 gap-10 sm:gap-16 xl:gap-20'>
        <img
          className='hidden md:block absolute inset-x-0 top-5'
          src={
            'https://ciseco-reactjs.vercel.app/static/media/VectorHIW.318309d7e47dcae15bad45e8d031e591.svg'
          }
          alt='vector'
        />
        {data.map((item: (typeof DEMO_DATA)[number], index: number) => (
          <div
            key={item.id}
            className='relative flex flex-col items-center max-w-xs mx-auto'
          >
            <NcImage
              containerClassName='mb-4 sm:mb-10 max-w-[140px] mx-auto'
              className='rounded-3xl'
              src={item.img}
            />
            <div className='text-center mt-auto space-y-5'>
              <Badge
                name={`Paso ${index + 1}`}
                color={
                  !index
                    ? 'red'
                    : index === 1
                    ? 'indigo'
                    : index === 2
                    ? 'yellow'
                    : 'purple'
                }
              />
              <h3 className='text-base font-semibold dark:text-neutral-300'>
                {item.title}
              </h3>
              <span className='block text-slate-600 dark:text-slate-400 text-sm leading-6'>
                {item.desc}
              </span>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default SectionHowItWork
