import {
  PayPalScriptProvider,
  PayPalButtons,
  usePayPalScriptReducer,
} from '@paypal/react-paypal-js'
import { useEffect } from 'react'

const amount = '2'
const currency = 'USD'
const style = { layout: 'vertical' }

const Paypal = ({ currency, showSpinner }: any) => {
  const [{ options, isPending }, dispatch] = usePayPalScriptReducer()

  useEffect(() => {
    dispatch({
      type: 'resetOptions',
      value: {
        ...options,
        currency: currency,
      },
    })
  }, [currency, showSpinner])

  return (
    <>
      {showSpinner && isPending && <div className='spinner' />}
      <PayPalButtons
        disabled={false}
        forceReRender={[amount, currency, style]}
        fundingSource={undefined}
        createOrder={(data, actions) => {
          return actions.order
            .create({
              purchase_units: [
                {
                  amount: {
                    currency_code: currency,
                    value: amount,
                  },
                },
              ],
            })
            .then((orderId) => {
              // Your code here after create the order
              return orderId
            })
        }}
        onApprove={function (data, actions) {
          return actions.order!.capture().then(function () {
            // Your code here after capture the order
          })
        }}
      />
    </>
  )
}

export default Paypal
