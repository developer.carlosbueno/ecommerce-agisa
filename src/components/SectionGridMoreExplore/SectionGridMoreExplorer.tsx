import CardCategory1 from '@components/General/CategoryCard/CategoryCard1'
import CardCategory4 from '@components/General/CategoryCard/CategoryCard4'
import Heading from '@components/Heading/Heading'
import NavItem2 from '@components/Nav/NavItem2'
import React, { FC } from 'react'
import Nav from '@components/shared/Nav'
import CardCategory6 from '@components/General/CategoryCard/CategoryCard6'
import { useCategoryBrandState } from '@/context/categoryBrand'

interface ExploreType {
  _id: string
  name: string
  desc: string
  image: string
  svgBg: string
  color?: string
}

export interface SectionGridMoreExploreProps {
  className?: string
  gridClassName?: string
  boxCard?: 'box1' | 'box4' | 'box6'
  data?: ExploreType[]
}

// export const DEMO_MORE_EXPLORE_DATA = [
//   {
//     id: 1,
//     name: 'Blusas',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/2.0fda32f45e4cd5e368ea.png',
//     // svgBg: explore1Svg,
//     color: 'bg-indigo-50',
//   },
//   {
//     id: 2,
//     name: 'Zapatos',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/explore2.63762511959d452238b7.png',
//     // svgBg: explore2Svg,
//     color: 'bg-slate-100/80',
//   },
//   {
//     id: 3,
//     name: 'Lentes',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/explore9.d3120ad348b834b14970.png',
//     // svgBg: explore7Svg,
//     color: 'bg-stone-100',
//   },
//   ,
//   {
//     id: 4,
//     name: 'Vestidos',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/explore6.329a9a9286612cf4e1b5.png',
//     // svgBg: explore9Svg,
//     color: 'bg-orange-50',
//   },
//   {
//     id: 5,
//     name: 'Cycling Jersey',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/explore9.d3120ad348b834b14970.png',
//     // svgBg: explore5Svg,
//     color: 'bg-blue-50',
//   },
//   {
//     id: 6,
//     name: 'Car Coat',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/explore9.d3120ad348b834b14970.png',
//     // svgBg: explore6Svg,
//     color: 'bg-orange-50',
//   },
//   {
//     id: 7,
//     name: 'Recycled Blanket',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/explore3.c9517d184abb769a2ac9.png',
//     // svgBg: explore3Svg,
//     color: 'bg-violet-50',
//   },
//   {
//     id: 8,
//     name: 'kid hats',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/explore9.d3120ad348b834b14970.png',
//     // svgBg: explore8Svg,
//     color: 'bg-blue-50',
//   },
//   {
//     id: 9,
//     name: 'Wool Jacket',
//     // desc: 'Manufacturar',
//     image:
//       'https://ciseco-reactjs.vercel.app/static/media/explore9.d3120ad348b834b14970.png',
//     // svgBg: explore4Svg,
//     color: 'bg-slate-100/80',
//   },
// ]

const SectionGridMoreExplore: FC<SectionGridMoreExploreProps> = ({
  className = '',
  boxCard = 'box4',
  gridClassName = 'grid-cols-1 md:grid-cols-2 xl:grid-cols-3',
}) => {
  const { categories } = useCategoryBrandState()
  const [tabActive, setTabActive] = React.useState('Man')

  const renderCard = (item: ExploreType) => {
    switch (boxCard) {
      case 'box1':
        return (
          <CardCategory1
            key={item._id}
            name={item.name}
            desc={item.desc}
            featuredImage={
              'https://ciseco-reactjs.vercel.app/static/media/2.0fda32f45e4cd5e368ea.png'
            }
          />
        )

      case 'box4':
        return (
          <CardCategory4
            name={item.name}
            desc={item.desc}
            _id={item._id}
            // bgSVG={item.svgBg}
            featuredImage={
              'https://ciseco-reactjs.vercel.app/static/media/2.0fda32f45e4cd5e368ea.png'
            }
            key={item._id}
            color={item.color}
          />
        )
      case 'box6':
        return (
          <CardCategory6
            name={item.name}
            desc={item.desc}
            // bgSVG={item.svgBg}
            featuredImage={
              'https://ciseco-reactjs.vercel.app/static/media/2.0fda32f45e4cd5e368ea.png'
            }
            key={item._id}
            color={item.color}
          />
        )

      default:
        return (
          <CardCategory4
            _id={item._id}
            name={item.name}
            desc={item.desc}
            // bgSVG={item.svgBg}
            featuredImage={
              'https://ciseco-reactjs.vercel.app/static/media/2.0fda32f45e4cd5e368ea.png'
            }
            key={item._id}
            color={item.color}
          />
        )
    }
  }

  const renderHeading = () => {
    return (
      <div>
        <Heading
          className='mb-12 lg:mb-14 text-neutral-900 dark:text-neutral-50'
          fontClass='text-3xl md:text-4xl 2xl:text-5xl font-semibold'
          isCenter
          desc=''
        >
          Empieza a{' '}
          <span className='underline decoration-lime-300'>explorar.</span>
        </Heading>
      </div>
    )
  }

  return (
    <div
      className={`nc-SectionGridMoreExplore relative ${className}`}
      data-nc-id='SectionGridMoreExplore'
    >
      {renderHeading()}
      <div className={`grid gap-4 md:gap-7 ${gridClassName}`}>
        {categories.map((item: any) => renderCard(item))}
      </div>
    </div>
  )
}

export default SectionGridMoreExplore
