import { FC } from 'react'
import { Link } from 'react-router-dom'
// import explore1Svg from 'images/collections/explore1.svg'
import { ArrowRightIcon } from '@heroicons/react/24/outline'

export interface CardCategory4Props {
  className?: string
  featuredImage?: string
  bgSVG?: string
  _id: string
  name: string
  desc: string
  color?: string
}

const CardCategory4: FC<CardCategory4Props> = ({
  className = '',
  featuredImage = '.',
  // bgSVG = explore1Svg,
  name,
  desc,
  color = 'bg-rose-50',
  _id,
}) => {
  return (
    <div
      className={`nc-CardCategory4 relative bg-green-50 hover:bg-green-400 hover:scale-105 cursor-pointer hover:text-slate-700  w-full aspect-w-12 aspect-h-11 h-[300px] rounded-sm shadow-sm border-black overflow-hidden  dark:bg-neutral-900 group  dark:hover:text-slate-500 hover:nc-shadow-lg transition-all ${className}`}
      data-nc-id='CardCategory4'
    >
      <div>
        <div className='absolute bottom-0 right-0 max-w-[280px] opacity-80'>
          {/* <img src={bgSVG} alt='' /> */}
        </div>

        <div className='absolute inset-5 sm:inset-8 flex flex-col justify-between group-hover:dark:hover:text-slate-500'>
          <div className='flex justify-between items-center'>
            <div
              className={`w-20 h-20 rounded-full overflow-hidden z-0 ${color}`}
            >
              <img src={featuredImage} alt='name' />
            </div>
            <span className='text-xs text-slate-700 dark:text-neutral-300 font-medium '>
              {Math.floor(Math.random() * 200 + 125)} products
            </span>
          </div>

          <div className=''>
            <span
              className={`block mb-2 text-sm text-slate-500 dark:text-slate-400`}
            >
              {desc}
            </span>
            <h2
              className={`text-2xl sm:text-3xl font-semibold dark:text-neutral-200`}
            >
              {name}
            </h2>
          </div>

          <Link
            to={`/products/category/${_id}`}
            className='flex items-center text-sm font-medium group-hover:text-primary-500 transition-colors dark:text-neutral-300'
          >
            <span>Ver productos</span>
            <ArrowRightIcon className='w-4 h-4 ml-2.5' />
          </Link>
        </div>
      </div>

      <Link to={`/products/category/${_id}`}></Link>
    </div>
  )
}

export default CardCategory4
