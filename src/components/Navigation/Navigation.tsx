import { useCategoryBrandState } from '@/context/categoryBrand'
import { NavigationItem } from './NavigationItem'

function Navigation() {
  const { categories } = useCategoryBrandState()

  return (
    <ul className='nc-Navigation flex items-center'>
      {categories.map((item) => (
        <NavigationItem key={item._id} menuItem={item} />
      ))}
    </ul>
  )
}

export default Navigation
