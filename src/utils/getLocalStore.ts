export const getLocalStore = (key: string) => {
  return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)!) : null
}

export const removetLocalStore = (key: string) => {
  return localStorage.removeItem(key)
}