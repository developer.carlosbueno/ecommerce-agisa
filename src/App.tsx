import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { HelmetProvider } from 'react-helmet-async'

// CSS
import './App.css'

// PAGES
import { Home } from '@pages/index'
import ProductDetail from './pages/ProductDetail/ProductDetail'
import { Layout } from './components'
import CartPage from './pages/Cart/Cart'
import CheckoutPage from './pages/Checkout/Checkout'
import PageLogin from './pages/Login/Login'
import AccountPage from './pages/Account/Account'
import Products from './pages/Products/Products'
import AccountOrder from './pages/Account/AccountOrder'
import AccountPass from './pages/Account/AccountPass'
import AccountBilling from './pages/Account/AccountBilling'
import AccountSavelists from './pages/Account/AccountSaveList'
import { CategoryBrandProvider } from './context/categoryBrand'
import { ProductsProvider } from './pages/Products/context'
import { CartProvider } from './context/cart'
import { OrderProvider } from './pages/Checkout/context'
import PageRegister from './pages/Register/Register'
import { AuthProvider } from './context/auth.context'

function App() {
  return (
    <HelmetProvider>
      <BrowserRouter>
        <AuthProvider>
          <CategoryBrandProvider>
            <CartProvider>
              <ProductsProvider>
                <OrderProvider>
                  <Layout>
                    <Routes>
                      <Route path='/home' element={<Home />} />
                      <Route path='/product/:id' element={<ProductDetail />} />
                      <Route path='/cart' element={<CartPage />} />
                      <Route path='/checkout' element={<CheckoutPage />} />
                      <Route path='/login' element={<PageLogin />} />
                      <Route path='/signup' element={<PageRegister />} />
                      <Route
                        path='/products/category/:category'
                        element={<Products />}
                      />

                      <Route path='/account' element={<AccountPage />} />
                      <Route
                        path='/account-my-order'
                        element={<AccountOrder />}
                      />
                      <Route
                        path='/account-change-password'
                        element={<AccountPass />}
                      />
                      <Route
                        path='/account-billing'
                        element={<AccountBilling />}
                      />
                      <Route
                        path='/account-savelists'
                        element={<AccountSavelists />}
                      />
                    </Routes>
                  </Layout>
                </OrderProvider>
              </ProductsProvider>
            </CartProvider>
          </CategoryBrandProvider>
        </AuthProvider>
      </BrowserRouter>
    </HelmetProvider>
  )
}

export default App
