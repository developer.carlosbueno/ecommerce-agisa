export interface Product {
  _id: string
  name: string
  price: number
  description: string
  sizes: ISizes[]
  category: string
  productType: string
  brand: string
  ofert: string
  images: string[]
  qty: number
}

export interface ISizes {
  name: string
  qty: number
}