
import { NavItemType } from "@/components/Navigation/NavigationItem/NavigationItem";
import nanoId from "@utils/nanoId";

export const MEGAMENU_TEMPLATES: NavItemType[] = [
  {
    id: nanoId(),
    href: "/",
    name: "Home Page",
    children: [
      { id: nanoId(), href: "/", name: "Home  1" },
      { id: nanoId(), href: "/home2", name: "Home  2", isNew: true },
      { id: nanoId(), href: "/", name: "Header  1" },
      { id: nanoId(), href: "/home2", name: "Header  2", isNew: true },
      { id: nanoId(), href: "/", name: "Coming Soon" },
    ],
  },
  {
    id: nanoId(),
    href: "/",
    name: "Shop Pages",
    children: [
      { id: nanoId(), href: "/page-collection", name: "Category Page 1" },
      { id: nanoId(), href: "/page-collection-2", name: "Category Page 2" },
      { id: nanoId(), href: "/product-detail", name: "Product Page 1" },
      { id: nanoId(), href: "/product-detail-2", name: "Product Page 2" },
      { id: nanoId(), href: "/cart", name: "Cart Page" },
      { id: nanoId(), href: "/checkout", name: "Checkout Page" },
    ],
  },
  {
    id: nanoId(),
    href: "/",
    name: "Other Pages",
    children: [
      { id: nanoId(), href: "/checkout", name: "Checkout Page" },
      { id: nanoId(), href: "/page-search", name: "Search Page" },
      { id: nanoId(), href: "/cart", name: "Cart Page" },
      { id: nanoId(), href: "/account", name: "Accout Page" },
      { id: nanoId(), href: "/account-my-order", name: "Order Page" },
      { id: nanoId(), href: "/subscription", name: "Subscription" },
    ],
  },
  {
    id: nanoId(),
    href: "/",
    name: "Blog Page",
    children: [
      { id: nanoId(), href: "/blog", name: "Blog Page" },
      { id: nanoId(), href: "/blog-single", name: "Blog Single" },
      { id: nanoId(), href: "/about", name: "About Page" },
      { id: nanoId(), href: "/contact", name: "Contact Page" },
      { id: nanoId(), href: "/login", name: "Login" },
      { id: nanoId(), href: "/signup", name: "Signup" },
    ],
  },
];

const OTHER_PAGE_CHILD: NavItemType[] = [
  {
    id: nanoId(),
    href: "/",
    name: "Home Demo 1",
  },
  {
    id: nanoId(),
    href: "/home2",
    name: "Home Demo 2",
  },
  {
    id: nanoId(),
    href: "/page-collection",
    name: "Category Pages",
    type: "dropdown",
    children: [
      {
        id: nanoId(),
        href: "/page-collection",
        name: "Category page 1",
      },
      {
        id: nanoId(),
        href: "/page-collection-2",
        name: "Category page 2",
      },
    ],
  },
  {
    id: nanoId(),
    href: "/product-detail",
    name: "Product Pages",
    type: "dropdown",
    children: [
      {
        id: nanoId(),
        href: "/product-detail",
        name: "Product detail 1",
      },
      {
        id: nanoId(),
        href: "/product-detail-2",
        name: "Product detail 2",
      },
    ],
  },
  {
    id: nanoId(),
    href: "/cart",
    name: "Cart Page",
  },
  {
    id: nanoId(),
    href: "/checkout",
    name: "Checkout Page",
  },
  {
    id: nanoId(),
    href: "/page-search",
    name: "Search Page",
  },
  {
    id: nanoId(),
    href: "/account",
    name: "Account Page",
  },
  {
    id: nanoId(),
    href: "/about",
    name: "Other Pages",
    type: "dropdown",
    children: [
      {
        id: nanoId(),
        href: "/about",
        name: "About",
      },
      {
        id: nanoId(),
        href: "/contact",
        name: "Contact us",
      },
      {
        id: nanoId(),
        href: "/login",
        name: "Login",
      },
      {
        id: nanoId(),
        href: "/signup",
        name: "Signup",
      },
      {
        id: nanoId(),
        href: "/subscription",
        name: "Subscription",
      },
    ],
  },
  {
    id: nanoId(),
    href: "/blog",
    name: "Blog Page",
    type: "dropdown",
    children: [
      {
        id: nanoId(),
        href: "/blog",
        name: "Blog Page",
      },
      {
        id: nanoId(),
        href: "/blog-single",
        name: "Blog Single",
      },
    ],
  },
];

export const NAVIGATION_DEMO_2: NavItemType[] = [
  {
    id: nanoId(),
    href: "/products",
    name: "Men",
  },
  {
    id: nanoId(),
    href: "/products",
    name: "Women",
  },
  {
    id: nanoId(),
    href: "/products",
    name: "Beauty",
  },

  {
    id: nanoId(),
    href: "/products",
    name: "Sport",
  },
  {
    id: nanoId(),
    href: "/page-search",
    name: "Templates",
    type: "megaMenu",
    children: MEGAMENU_TEMPLATES,
  },
  {
    id: nanoId(),
    href: "/page-search",
    name: "Explore",
    type: "dropdown",
    children: OTHER_PAGE_CHILD,
  },
];
