import { ISizes } from "@/models/Product";
import { productImgs } from "@components/Contains/fakeData";

// import productVariantImg2 from "images/products/v2.jpg";
// import productVariantImg3 from "images/products/v3.jpg";
// import productVariantImg4 from "images/products/v4.jpg";
// import productVariantImg5 from "images/products/v5.jpg";
// import productVariantImg6 from "images/products/v6.jpg";
// //
// import productSport1 from "images/products/sport-1.png";
// import productSport2 from "images/products/sport-2.png";
// import productSport3 from "images/products/sport-3.png";
// import productSport4 from "images/products/sport-4.png";
// import productSport5 from "images/products/sport-5.png";
// import productSport6 from "images/products/sport-6.png";
// import productSport7 from "images/products/sport-7.png";
// import productSport8 from "images/products/sport-8.png";

//

export interface ProductVariant {
  _id: number;
  name: string;
  thumbnail?: string;
  color?: string;
  featuredImage: string;
}

export interface Product {
  _id: string
  name: string
  price: number
  description: string
  sizes: ISizes[],
  qty: number,
  category: string
  brand: string
  ofert: string
  images: string[]
}

// export interface Product {
//   _id: 'n'umber;
//   name: string;
//   price: number;
//   images: string;
//   description: string;
// brand: '',
//   ofert: '' 
// category: string;
//   tags: string[];
// link: "/product-detail/";
// variants ?: ProductVariant[];
// variantType ?: "color" | "image";
//   sizes?: string[];
// allOfSizes ?: string[];
// status ?: "New in" | "limited edition" | "Sold Out" | "50% Discount";
// }

export const sizes: ISizes[] = [
  { name: 'sm', qty: 0 },
  { name: 'md', qty: 0 },
  { name: 'lg', qty: 0 },
  { name: 'xl', qty: 0 },
  { name: 'xxl', qty: 0 },
]


export const DEMO_VARIANTS: ProductVariant[] = [
  {
    _id: 1,
    name: "Black",
    thumbnail: 'https://ciseco-reactjs.vercel.app/static/media/1.a586787f3de7735e65d3.png',
    featuredImage: productImgs[0],
  },
  {
    _id: 2,
    name: "White",
    thumbnail: 'https://ciseco-reactjs.vercel.app/static/media/1.a586787f3de7735e65d3.png',
    featuredImage: productImgs[1],
  },
  {
    _id: 3,
    name: "Orange",
    thumbnail: 'https://ciseco-reactjs.vercel.app/static/media/1.a586787f3de7735e65d3.png',
    featuredImage: productImgs[2],
  },
  {
    _id: 4,
    name: "Sky Blue",
    thumbnail: 'https://ciseco-reactjs.vercel.app/static/media/1.a586787f3de7735e65d3.png',
    featuredImage: productImgs[3],
  },
  {
    _id: 5,
    name: "Natural",
    thumbnail: 'https://ciseco-reactjs.vercel.app/static/media/1.a586787f3de7735e65d3.png',
    featuredImage: productImgs[4],
  },
];
// const DEMO_VARIANT_COLORS: ProductVariant[] = [
//   {
//     id: 1,
//     name: "Violet",
//     color: "bg-violet-400",
//     featuredImage: productImgs[0],
//   },
//   {
//     id: 2,
//     name: "Yellow",
//     color: "bg-yellow-400",
//     featuredImage: productImgs[1],
//   },
//   {
//     id: 3,
//     name: "Orange",
//     color: "bg-orange-400",
//     featuredImage: productImgs[2],
//   },
//   {
//     id: 4,
//     name: "Sky Blue",
//     color: "bg-sky-400",
//     featuredImage: productImgs[3],
//   },
//   {
//     id: 5,
//     name: "Green",
//     color: "bg-green-400",
//     featuredImage: productImgs[4],
//   },
// ];

export const PRODUCTS: Product[] = [
  {
    _id: '1',
    name: "Rey Nylon Backpack",
    description: "Brown cockroach wings",
    price: 74,
    images: [productImgs[16]],
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // link: "/product-detail/",
    // // variants: DEMO_VARIANTS,
    // variantType: "image",
    sizes: sizes,
    // allOfSizes: ["XS", "S", "M", "L", "XL", "2XL", "3XL"],
    // status: "New in",
  },
  {
    _id: '2',
    name: 'Round Buckle 1" Belt',
    description: "Classic green",
    price: 68,
    images: productImgs,
    brand: '',
    ofert: '',
    qty: 0,
    sizes: sizes,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // link: "/product-detail/",
    // variants: DEMO_VARIANT_COLORS,
    // variantType: "color",
    // status: "50% Discount",
  },
  {
    _id: '3',
    name: "Waffle Knit Beanie",
    description: "New blue aqua",
    price: 132,
    images: productImgs,
    sizes,
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // link: "/product-detail/",
    // // variants: DEMO_VARIANTS,
    // variantType: "image",
    // allOfSizes: ["S", "M", "L", "XL", "2XL", "3XL"],
  },
  {
    _id: '4',
    name: "Travel Pet Carrier",
    description: "Dark pink 2023",
    price: 28,
    images: productImgs,
    sizes,
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // variants: DEMO_VARIANT_COLORS,
    // variantType: "color",
    // link: "/product-detail/",
    // status: "Sold Out",
  },
  {
    _id: '5',
    name: "Leather Gloves",
    description: "Perfect mint green",
    price: 42,
    images: productImgs,
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // // variants: DEMO_VARIANTS,
    // variantType: "image",
    sizes: sizes,
    // allOfSizes: ["XS", "S", "M", "L", "XL", "2XL", "3XL"],
    // link: "/product-detail/",
  },
  {
    _id: '6',
    name: "Hoodie Sweatshirt",
    description: "New design 2023",
    price: 30,
    images: productImgs,
    sizes,
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // variantType: "color",
    // variants: DEMO_VARIANT_COLORS,
    // link: "/product-detail/",
  },
  {
    _id: '7',
    name: "Wool Cashmere Jacket",
    description: "Matte black",
    price: 12,
    images: productImgs,
    sizes,
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // // variants: DEMO_VARIANTS,
    // variantType: "image",
    // link: "/product-detail/",
    // status: "New in",
  },
  {
    _id: '8',
    name: "Ella Leather Tote",
    description: "Cream pink",
    price: 145,
    images: [productImgs[7]],
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // // variants: DEMO_VARIANTS,
    // variantType: "image",
    sizes: sizes,
    // allOfSizes: ["XS", "S", "M", "L", "XL", "2XL", "3XL"],
    // link: "/product-detail/",
    // status: "limited edition",
  },
];

export const SPORT_PRODUCTS: Product[] = [
  {
    _id: '1',
    name: "Mastermind Toys",
    description: "Brown cockroach wings",
    price: 74,
    images: ['https://ciseco-reactjs.vercel.app/static/media/sport-6.38e1be2735a4eab88fc6.png'],
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // link: "/product-detail/",
    // variants: DEMO_VARIANT_COLORS,
    // variantType: "color",
    sizes: sizes,
    // allOfSizes: ["XS", "S", "M", "L", "XL", "2XL", "3XL"],
    // status: "New in",
  },
  {
    _id: '2',
    name: "Jump Rope Kids",
    description: "Classic green",
    price: 68,
    images: ['https://ciseco-reactjs.vercel.app/static/media/sport-6.38e1be2735a4eab88fc6.png'],
    brand: '',
    ofert: '',
    qty: 0,
    sizes,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // link: "/product-detail/",
    // variants: DEMO_VARIANT_COLORS,
    // variantType: "color",
    // status: "50% Discount",
  },
  {
    _id: '3',
    name: "Tee Ball Beanie",
    description: "New blue aqua",
    price: 132,
    images: ['https://ciseco-reactjs.vercel.app/static/media/sport-6.38e1be2735a4eab88fc6.png'],
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // link: "/product-detail/",
    // // variants: DEMO_VARIANTS,
    // variantType: "image",
    sizes: sizes,
    // allOfSizes: ["S", "M", "L", "XL", "2XL", "3XL"],
  },
  {
    _id: '4',
    name: "Rubber Table Tennis",
    description: "Dark pink 2023",
    price: 28,
    images: ['https://ciseco-reactjs.vercel.app/static/media/sport-6.38e1be2735a4eab88fc6.png'],
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    sizes,
    // tags: ["tag1", "tag2"],
    // variants: DEMO_VARIANT_COLORS,
    // variantType: "color",
    // link: "/product-detail/",
    // status: "Sold Out",
  },
  {
    _id: '5',
    name: "Classic Blue Rugby",
    description: "Perfect mint green",
    price: 42,
    images: ['https://ciseco-reactjs.vercel.app/static/media/sport-6.38e1be2735a4eab88fc6.png'],
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // // variants: DEMO_VARIANTS,
    // variantType: "image",
    sizes: sizes,
    // allOfSizes: ["XS", "S", "M", "L", "XL", "2XL", "3XL"],
    // link: "/product-detail/",
  },
  {
    _id: '6',
    name: "Manhattan Toy WRT",
    description: "New design 2023",
    price: 30,
    images: ['https://ciseco-reactjs.vercel.app/static/media/sport-6.38e1be2735a4eab88fc6.png'],
    brand: '',
    ofert: '',
    sizes,
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // variantType: "color",
    // variants: DEMO_VARIANT_COLORS,
    // link: "/product-detail/",
  },
  {
    _id: '7',
    name: "Tabletop Football ",
    description: "Matte black",
    price: 12,
    images: ['https://ciseco-reactjs.vercel.app/static/media/sport-6.38e1be2735a4eab88fc6.png'],
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    sizes,
    // tags: ["tag1", "tag2"],
    // // variants: DEMO_VARIANTS,
    // variantType: "image",
    // link: "/product-detail/",
    // status: "New in",
  },
  {
    _id: '8',
    name: "Pvc Catching Toy",
    description: "Cream pink",
    price: 145,
    images: ['https://ciseco-reactjs.vercel.app/static/media/sport-6.38e1be2735a4eab88fc6.png'],
    brand: '',
    ofert: '',
    qty: 0,
    category: "Category 1",
    // tags: ["tag1", "tag2"],
    // variants: DEMO_VARIANT_COLORS,
    // variantType: "color",
    sizes: sizes,
    // allOfSizes: ["XS", "S", "M", "L", "XL", "2XL", "3XL"],
    // link: "/product-detail/",
    // status: "limited edition",
  },
];
