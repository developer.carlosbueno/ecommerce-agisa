import { DataService } from "@config/api";

export interface IBrand {
  name: string
  _id: string
}

export interface ICategory {
  name: string
  href: string
  _id: string
}


export const getBrands = async (): Promise<IBrand[]> => {
  try {
    const { data } = await DataService.get('/brands')
    return data as IBrand[]
  } catch (error: any) {
    throw new Error(error.data.response.error || error.message)
  }
}

// Category

export const getCategories = async (): Promise<ICategory[]> => {
  try {
    const { data } = await DataService.get('/categories')
    return data as ICategory[]
  } catch (error: any) {
    throw new Error(error.data.response.error || error.message)
  }
}
