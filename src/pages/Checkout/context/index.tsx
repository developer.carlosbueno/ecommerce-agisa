import React, {
  ReactElement,
  createContext,
  useContext,
  useState,
  useEffect,
} from 'react'
import { ContactInfo, Order, Shipping } from '../models/Order'

// Services
import { getOrders, getOneOrder, postOrder } from '../services'
import { useCartState } from '@/context/cart'
import toast from 'react-hot-toast'
import { useAuthState } from '@/context/auth.context'
import { useNavigate } from 'react-router-dom'
// import { toast } from '../../../App'

const initialState: InitialStateProps = {
  orders: [],
  contactInfo: { email: '', phone: '' },
  shipping: {
    fullname: '',
    province: 'SANTO DOMINGO',
    agent: '',
    street1: '',
    street2: '',
    help: '',
  },
  order: null,
  paymentMethod: 'Tarjeta',
  selectOrder: () => {},
  addOrder: () => {},
  removeOrder: () => {},
  setSearch: () => {},
  setOrder: () => {},
  changeContactInfo: () => {},
  setPaymentMethod: () => {},
  changeShipping: () => {},
}

const OrderContext = createContext(initialState)

export interface InventoryProviderProps {
  children: ReactElement
}

export const OrderProvider: React.FC<InventoryProviderProps> = ({
  children,
}) => {
  const navigate = useNavigate()
  const { cart } = useCartState()
  const { auth } = useAuthState()

  const [orders, setOrders] = useState<Order[]>(initialState.orders)
  const [paymentMethod, setPaymentMethod] = useState<string>(
    initialState.paymentMethod
  )
  const [contactInfo, setContactInfo] = useState<ContactInfo>(
    initialState.contactInfo
  )
  const [shipping, setShipping] = useState<Shipping>(initialState.shipping)
  const [searchOrders, setSearchOrders] = useState<Order[]>([])
  const [search, setSearch] = useState<string>('')
  const [order, setOrder] = useState<Order | null>(initialState.order)
  const [selectedOrder, setSelectedOrder] = useState('')

  let activeOrder = search ? searchOrders : orders

  useEffect(() => {
    getInitialState()
    setContactInfo({ email: auth?.email || '', phone: '' })
    setShipping({
      fullname: auth?.fullname || '',
      province: 'SANTO DOMINGO',
      agent: '',
      street1: '',
      street2: '',
      help: '',
    })
  }, [])

  useEffect(() => {
    if (search) {
      handleSearchOrder()
    }
  }, [search])

  useEffect(() => {
    if (selectedOrder) {
      fetchOneOrder()
    }
  }, [selectedOrder])

  const handleSearchOrder = () => {
    let searched = orders.filter((p) =>
      p._id!.toLowerCase().includes(search.toLowerCase())
    )

    setSearchOrders(searched)
  }

  const getInitialState = async () => {
    try {
      const ordersData = await getOrders()
      setOrders(ordersData)
    } catch (error) {}
  }

  const fetchOneOrder = async () => {
    try {
      const orderData = await getOneOrder(selectedOrder)
      setOrder(orderData)
    } catch (error) {}
  }

  const addOrder = async () => {
    try {
      if (!cart.length) {
        return toast.error('No tienes productos en el carrito!')
      }

      if (!shipping.province || !shipping.fullname) {
        return toast.error('Llena la informacion del envio!')
      }

      if (shipping.province === 'SANTO DOMINGO' && !shipping.street1) {
        return toast.error('Llena la direccion del envio!')
      }

      if (!contactInfo.email || !contactInfo.phone) {
        return toast.error('Llena la informacion de contacto!')
      }

      if (!paymentMethod) {
        return toast.error('Elige un metodo de pago!')
      }

      const order = await postOrder({
        client: auth?._id || shipping.fullname,
        orderItems: cart.map((i) => ({
          product: i.product._id,
          qty: i.qty,
          size: i.size,
        })),
        shippingAddress: shipping,
        paymentMethod,
        shippingPrice: 200,
        taxPrice: 65,
        totalPrice: cart.reduce(
          (acc, cur) => acc + cur.qty * cur.product.price,
          0
        ),
      })
      toast.success('Compra realizada!')
      navigate('/home')
      return order
    } catch (error: any) {
      toast.error(error.message)
    }
  }

  const changeShipping = (key: string, value: string) => {
    let copy: any = { ...shipping }

    copy[key] = value

    setShipping(copy)
    console.log(copy)
    setTimeout(() => {
      console.log(shipping)
    }, 1000)
  }

  const changeContactInfo = (key: string, value: string) => {
    console.log('CONTACT', contactInfo)
    setContactInfo((prev) => ({ ...prev, [key]: value }))
  }

  const removeOrder = (id: string) => {
    setOrders((prev) => prev.filter((i) => i._id !== id))
  }

  return (
    <OrderContext.Provider
      value={{
        orders: activeOrder,
        selectOrder: setSelectedOrder,
        shipping,
        contactInfo,
        paymentMethod,
        setPaymentMethod,
        changeContactInfo,
        changeShipping,
        order,
        addOrder,
        removeOrder,
        setOrder,
        setSearch,
      }}
    >
      {children}
    </OrderContext.Provider>
  )
}

export const useOrderState = () => useContext(OrderContext)

export interface InitialStateProps {
  orders: Order[]
  shipping: Shipping
  contactInfo: ContactInfo
  order: Order | null
  paymentMethod: string
  selectOrder: (id: string) => void
  addOrder: () => void
  removeOrder: (id: string) => void
  setSearch: (string: string) => void
  setOrder: (product: Order) => void
  changeContactInfo: (key: string, value: string) => void
  changeShipping: (key: string, value: string) => void
  setPaymentMethod: (value: string) => void
}
