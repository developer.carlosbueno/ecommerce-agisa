import { OrderItem, Shipping } from "../models/Order"

export interface OrderDTO {
  client: string
  orderItems: OrderItem[]
  shippingAddress: Shipping
  paymentMethod: string
  taxPrice: number
  shippingPrice: number
  totalPrice: number
}
