import { DataService } from "../../../config/api";
import { OrderDTO } from "../dto/OrderDTO";
import { Order } from "../models/Order";

export const getOrders = async (): Promise<Order[]> => {
  try {
    const { data } = await DataService.get('/orders')
    return data as Order[]
  } catch (error: any) {
    throw new Error(error.response.data.message || error.message)
  }
}

export const getOneOrder = async (id: string): Promise<Order> => {
  try {
    const { data } = await DataService.get('/orders/' + id)
    return data as Order
  } catch (error: any) {
    throw new Error(error.response.data.message || error.message)
  }
}

export const postOrder = async (body: OrderDTO): Promise<Order> => {
  try {
    const { data } = await DataService.post('/orders', body)
    console.log('error', data)
    return data as Order
  } catch (error: any) {
    console.log('error', error)
    throw new Error(error.response.data.message || error.message)
  }
}

export const updateOrder = async (id: string, body: Order): Promise<Order> => {
  try {
    const { data } = await DataService.put(`/orders/:${id}`, body)
    return data as Order
  } catch (error: any) {
    throw new Error(error.response.data.message || error.message)
  }
}

export const deliverOrder = async (id: string): Promise<Order> => {
  try {
    const { data } = await DataService.patch(`/orders/${id}/deliver`)
    return data as Order
  } catch (error: any) {
    throw new Error(error.response.data.message || error.message)
  }
}

export const removeOrder = async (id: string): Promise<Order> => {
  try {
    const { data } = await DataService.delete(`/orders/:${id}`)
    return data as Order
  } catch (error: any) {
    throw new Error(error.response.data.message || error.message)
  }
}