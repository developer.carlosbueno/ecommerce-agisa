export interface Order {
  _id?: string
  client: string
  orderItems: OrderItem[]
  shippingAddress: Shipping
  paymentMethod: string
  taxPrice: number
  shippingPrice: number
  totalPrice: number
  isDelivered: boolean
  deliveredAt: number
  completed: boolean
  createdAt: string
  updatedAt: string
}



export interface OrderItem {
  product: string
  qty: number
  size: string
}

export interface ContactInfo {
  email: string
  phone: string
}

export interface Shipping {
  province: string
  fullname: string
  street1?: string
  street2?: string
  help?: string
  agent?: string
}