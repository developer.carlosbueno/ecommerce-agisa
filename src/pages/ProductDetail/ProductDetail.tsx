import BagIcon from '@/components/Products/BagIcon'
import IconDiscount from '@/components/Products/IconDiscount'
import LikeButton from '@/components/Products/LikeButton'
import Prices from '@/components/Products/Prices'
import SectionSliderProductCard from '@/components/SectionSliderProductCard'
import { ButtonPrimary } from '@/components/shared/Button'
import { SERVER_URL_IMAGE } from '@/config/config'
import { useCartState } from '@/context/cart'
import { DEMO_VARIANTS, PRODUCTS } from '@data/data'
import {
  ClockIcon,
  NoSymbolIcon,
  SparklesIcon,
} from '@heroicons/react/24/outline'
import { StarIcon } from '@heroicons/react/24/solid'
import React, { FC, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useProductsState } from '../Products/context'
import ModalViewAllReviews from './components/ModalViewAllReviews'
import Policy from './components/Policy'
import Select from '@/components/shared/Inputs/Select'
import Badge from '@/components/shared/Badge'
import toast from 'react-hot-toast'
import Input from '@/components/shared/Inputs/Input'
import NcInputNumber from '@/components/shared/Inputs/InputNumber'

export interface ProductDetailProps {
  className?: string
}

const ProductDetail: FC<ProductDetailProps> = ({ className = '' }) => {
  const { addToCart } = useCartState()
  const { getProduct, product } = useProductsState()
  const { id } = useParams()

  const variants = DEMO_VARIANTS
  const status = 'New in'

  const [sizeSelected, setSizeSelected] = React.useState(
    product?.sizes[0]?.name
  )
  const [isOpenModalViewAllReviews, setIsOpenModalViewAllReviews] =
    useState(false)

  const [qty, setQty] = useState(1)

  useEffect(() => {
    getProduct(id!)
  }, [id])

  const notifyAddTocart = () => {
    addToCart(product!, qty, sizeSelected! || '')
  }

  const renderVariants = () => {
    if (!variants || !variants.length) {
      return null
    }

    return (
      <div className='dark:text-neutral-200'>
        <label htmlFor=''>
          <span className='text-sm font-medium'>
            Color:
            <span className='ml-1 font-semibold'>{variants[0].name}</span>
          </span>
        </label>
      </div>
    )
  }

  const renderSizeList = () => {
    if (product?.productType !== 'sizes' || !product?.sizes.length) {
      return null
    }
    return (
      <div className='dark:text-neutral-200'>
        <div className='flex items-center font-medium text-sm'>
          <label htmlFor='' className='block mr-6'>
            Size:
          </label>
          <div className='w-2/6'>
            <Select
              className='w-full text-center'
              onChange={(e) => setSizeSelected(e.target.value)}
            >
              {product?.sizes.map((i) => (
                <option value={i.name} label={i.name} />
              ))}
            </Select>
          </div>
        </div>
      </div>
    )
  }

  const renderStatus = () => {
    if (!status) {
      return null
    }
    const CLASSES =
      'absolute top-3 left-3 px-2.5 py-1.5 text-xs bg-white dark:bg-slate-900 nc-shadow-lg rounded-full flex items-center justify-center text-slate-700 text-slate-900 dark:text-slate-300'
    if (status === 'New in') {
      return (
        <div className={CLASSES}>
          <SparklesIcon className='w-3.5 h-3.5' />
          <span className='ml-1 leading-none'>{status}</span>
        </div>
      )
    }
    if (status === '50% Discount') {
      return (
        <div className={CLASSES}>
          <IconDiscount className='w-3.5 h-3.5' />
          <span className='ml-1 leading-none'>{status}</span>
        </div>
      )
    }
    if (status === 'Sold Out') {
      return (
        <div className={CLASSES}>
          <NoSymbolIcon className='w-3.5 h-3.5' />
          <span className='ml-1 leading-none'>{status}</span>
        </div>
      )
    }
    if (status === 'limited edition') {
      return (
        <div className={CLASSES}>
          <ClockIcon className='w-3.5 h-3.5' />
          <span className='ml-1 leading-none'>{status}</span>
        </div>
      )
    }
    return null
  }

  const renderSectionContent = () => {
    return (
      <div className='space-y-7 2xl:space-y-8'>
        {/* ---------- 1 HEADING ----------  */}
        <div className='dark:text-neutral-200'>
          <h2 className='text-2xl sm:text-3xl font-semibold'>
            {product?.name}
          </h2>

          <div className='flex items-center mt-5 space-x-4 sm:space-x-5'>
            {/* <div className="flex text-xl font-semibold">$112.00</div> */}
            <Prices
              contentClass='py-1 px-2 md:py-1.5 md:px-3 text-lg font-semibold'
              price={product?.price}
            />

            <div className='h-7 border-l border-slate-300 dark:border-slate-700'></div>

            <div className='flex items-center'>
              <a
                href='#reviews'
                className='flex items-center text-sm font-medium'
              >
                <StarIcon className='w-5 h-5 pb-[1px] text-yellow-400' />
                <div className='ml-1.5 flex'>
                  <span>4.9</span>
                  <span className='block mx-2'>·</span>
                  <span
                    onClick={() => setIsOpenModalViewAllReviews(true)}
                    className='text-slate-600 dark:text-slate-400 underline'
                  >
                    142 reviews
                  </span>
                </div>
              </a>
              <span className='hidden sm:block mx-2.5'>·</span>
              <div className='hidden sm:flex items-center text-sm'>
                <SparklesIcon className='w-3.5 h-3.5' />
                <span className='ml-1 leading-none'>{status}</span>
              </div>
            </div>
          </div>
        </div>

        {/* ---------- 3 VARIANTS AND SIZE LIST ----------  */}
        <div className=''>{renderVariants()}</div>
        <div className=''>{renderSizeList()}</div>

        {!product?.sizes.length && product?.qty == 0 && (
          <Badge color='red' name='Agotados' />
        )}

        {(product?.sizes.length || product?.qty! > 0) && (
          <NcInputNumber
            onDecrement={() => setQty((prev) => prev - 1)}
            onIncrement={() => setQty((prev) => prev + 1)}
            max={product?.qty || 5}
            value={qty}
          />
        )}

        {/*  ---------- 4  QTY AND ADD TO CART BUTTON */}
        <div className='flex space-x-3.5'>
          <ButtonPrimary
            className='flex-1 flex-shrink-0'
            onClick={notifyAddTocart}
          >
            <BagIcon className='hidden sm:inline-block w-5 h-5 mb-0.5' />
            <span className='ml-3'>Add to cart</span>
          </ButtonPrimary>
        </div>

        {/*  */}
        <hr className=' 2xl:!my-10 border-slate-200 dark:border-slate-700'></hr>
        {/*  */}

        {/* ---------- 5 ----------  */}
        {/* <AccordionInfo /> */}

        {/* ---------- 6 ----------  */}
        <div className='hidden xl:block'>
          <Policy />
        </div>
      </div>
    )
  }

  return (
    <div className={`nc-ProductDetailPage ${className}`}>
      {/* MAIn */}
      <main className='container mt-5 lg:mt-11'>
        <div className='lg:flex'>
          {/* CONTENT */}
          <div className='w-full lg:w-[55%] '>
            {/* HEADING */}
            <div className='relative'>
              <div className='aspect-w-16 aspect-h-16'>
                <img
                  src={SERVER_URL_IMAGE + product?.images[0]}
                  className='w-full rounded-2xl object-cover'
                  alt='product detail 1'
                />
              </div>
              {renderStatus()}
              {/* META FAVORITES */}
              <LikeButton className='absolute right-3 top-3 ' />
            </div>
          </div>

          {/* SIDEBAR */}
          <div className='w-full lg:w-[45%] pt-10 lg:pt-0 lg:pl-7 xl:pl-9 2xl:pl-10'>
            {renderSectionContent()}
          </div>
        </div>

        {/* DETAIL AND REVIEW */}
        <div className='mt-12 sm:mt-16 space-y-10 sm:space-y-16 mb-20'>
          <div className='block xl:hidden'>
            <Policy />
          </div>

          <hr className='border-slate-200 dark:border-slate-700' />

          {/* OTHER SECTION */}
          <SectionSliderProductCard
            heading='Productos relacionados'
            subHeading=''
            headingFontClassName='text-2xl font-semibold'
            headingClassName='mb-10 text-neutral-900 dark:text-neutral-50'
          />
        </div>
      </main>

      {/* MODAL VIEW ALL REVIEW */}
      <ModalViewAllReviews
        show={isOpenModalViewAllReviews}
        onCloseModalViewAllReviews={() => setIsOpenModalViewAllReviews(false)}
      />
    </div>
  )
}

export default ProductDetail
