import { FC } from 'react'
import { ButtonPrimary } from '@/components/shared/Button'
import Logo from '@/components/shared/Logo'

export interface SectionPromo2Props {
  className?: string
}

const SectionPromo2: FC<SectionPromo2Props> = ({ className = 'lg:pt-10' }) => {
  return (
    <div className={`nc-SectionPromo2 ${className}`}>
      <div className='relative flex flex-col lg:flex-row lg:justify-end bg-yellow-50 dark:bg-slate-800 rounded-2xl sm:rounded-[40px] p-4 pb-0 sm:p-5 sm:pb-0 lg:p-24'>
        <div className='absolute inset-0'>
          <img
            className='absolute w-full h-full object-contain dark:opacity-5'
            src={
              'https://ciseco-reactjs.vercel.app/static/media/Moon.bf27dc577d1acccaba48430d353dbbe0.svg'
            }
            alt='backgroundLineSvg'
          />
        </div>

        <div className='lg:w-[45%] max-w-lg relative'>
          <Logo className='w-28' />
          <h2 className='font-semibold text-3xl sm:text-4xl xl:text-5xl 2xl:text-6xl mt-6 sm:mt-10 !leading-[1.13] tracking-tight'>
            Special offer <br />
            in kids products
          </h2>
          <span className='block mt-6 text-slate-500 dark:text-slate-400'>
            Fashion is a form of self-expression and autonomy at a particular
            period and place.
          </span>
          <div className='flex space-x-2 sm:space-x-5 mt-6 sm:mt-12'>
            <ButtonPrimary
              href='/page-search'
              className='dark:bg-slate-200 dark:text-slate-900'
            >
              Discover more
            </ButtonPrimary>
          </div>
        </div>
        <div className='relative block lg:absolute lg:left-0 lg:bottom-0 mt-10 lg:mt-0 max-w-xl lg:max-w-[calc(55%-40px)]'>
          <img
            src={
              'https://ciseco-reactjs.vercel.app/static/media/promo2.9278c86b3a2f01d6666e.png'
            }
          />
        </div>
      </div>
    </div>
  )
}

export default SectionPromo2
