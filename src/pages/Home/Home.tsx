import React from 'react'
import './styles.css'

// Components
import SectionHero3 from '@/components/SectionHero/SectionHero3'
import SectionSliderProductCard from '@/components/SectionSliderProductCard'

import { PRODUCTS, SPORT_PRODUCTS } from '@data/data'
import SectionHowItWork from '@/components/SectionHowItWorks'
import DiscoverMoreSlider from '@/components/DiscoverMoreSlider'
import BackgroundSection from '@/components/BackgroundSection'
import SectionGridMoreExplore from '@/components/SectionGridMoreExplore/SectionGridMoreExplorer'
import SectionGridFeatureItems from './components/SectionGridFeatureItems'
import { useProductsState } from '../Products/context'

interface IHomeProps {
  children?: React.ReactNode
}

const Home: React.FC<IHomeProps> = (props) => {
  const { products } = useProductsState()

  return (
    <div className='dark:bg-slate-900'>
      {/* HERO */}
      <SectionHero3 />
      <div className='container overflow-hidden relative space-y-24 my-24 lg:space-y-32 lg:my-32'>
        {/* SECTION SLIDER */}
        <SectionSliderProductCard
          data={[
            products[0],
            products[0],
            products[0],
            products[0],
            products[0],
          ]}
        />

        {/* HOW IT WORKS */}
        <SectionHowItWork />
        {/* SECTION */}

        <div className=' py-24 lg:py-32'>
          <SectionGridMoreExplore gridClassName='grid-cols-1 md:grid-cols-2 xl:grid-cols-3' />
        </div>

        {/* SECTION */}
        {/* <SectionGridFeatureItems /> */}
      </div>
    </div>
  )
}

export default Home
