import Label from '@/components/Label'
import { ButtonPrimary } from '@/components/shared/Button'
import Input from '@/components/shared/Inputs/Input'
import CommonLayout from './components/CommonLayout'

const AccountPass = () => {
  return (
    <div>
      <CommonLayout>
        <div className='space-y-10 sm:space-y-12'>
          {/* HEADING */}
          <h2 className='text-2xl sm:text-3xl font-semibold dark:text-slate-300'>
            Actualizar contraseña
          </h2>
          <div className=' max-w-xl space-y-6'>
            <div>
              <Label>Contraseña actual</Label>
              <Input type='password' className='mt-1.5' />
            </div>
            <div>
              <Label>Nueva contraseña</Label>
              <Input type='password' className='mt-1.5' />
            </div>
            <div>
              <Label>Confirmar contraseña</Label>
              <Input type='password' className='mt-1.5' />
            </div>
            <div className='pt-2'>
              <ButtonPrimary>Actualizar contraseña</ButtonPrimary>
            </div>
          </div>
        </div>
      </CommonLayout>
    </div>
  )
}

export default AccountPass
