import Message from '@/components/shared/Message'
import CommonLayout from './components/CommonLayout'

const AccountSavelists = () => {
  const renderSection1 = () => {
    return (
      <div className='space-y-10 sm:space-y-12'>
        <div>
          <h2 className='text-2xl sm:text-3xl font-semibold dark:text-slate-300'>
            Productos favoritos
          </h2>
        </div>
        {/* 
        <div className='grid grid-cols-1 gap-6 md:gap-8 sm:grid-cols-2 lg:grid-cols-3 '>
          {PRODUCTS.filter((_, i) => i < 6).map((stay) => (
            <ProductCard key={stay._id} data={stay} />
          ))}
        </div> */}
        <Message
          message='No has hecho tu primera compra. Busca tu producto deasado y ordenalo'
          type='error'
        />
        <div className='flex !mt-20 justify-center items-center'>
          {/* <ButtonSecondary loading>Show me more</ButtonSecondary> */}
        </div>
      </div>
    )
  }

  return (
    <div>
      <CommonLayout>{renderSection1()}</CommonLayout>
    </div>
  )
}

export default AccountSavelists
