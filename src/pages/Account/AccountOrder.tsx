import Prices from '@/components/Products/Prices'
import { PRODUCTS } from '@data/data'
import ButtonSecondary from '@/components/shared/Button/ButtonSecondary'
import CommonLayout from './components/CommonLayout'
import { useAuthState } from '@/context/auth.context'
import { useEffect } from 'react'
import { Product } from '@/models/Product'
import Message from '@/components/shared/Message'

const AccountOrder = () => {
  const { authOrders, getMyOrders } = useAuthState()

  useEffect(() => {
    getMyOrders()
  }, [])

  const renderProductItem = (product: Product, qty: number) => {
    console.log('p', product)
    const { images, name, _id, price } = product
    return (
      <div key={_id} className='flex py-4 sm:py-7 last:pb-0 first:pt-0'>
        <div className='h-24 w-16 sm:w-20 flex-shrink-0 overflow-hidden rounded-xl bg-slate-100'>
          <img
            src={'http://localhost:3000/' + images[0]}
            alt={name}
            className='h-full w-full object-cover object-center'
          />
        </div>

        <div className='ml-4 flex flex-1 flex-col'>
          <div>
            <div className='flex justify-between '>
              <div>
                <h3 className='text-base font-medium line-clamp-1 dark:text-slate-400'>
                  {name}
                </h3>
                <p className='mt-1 text-sm text-slate-500 dark:text-slate-400'>
                  <span>{'Natural'}</span>
                  <span className='mx-2 border-l border-slate-200 dark:border-slate-700 h-4'></span>
                  {/* <span>{size}</span> */}
                </p>
              </div>
              <Prices className='mt-0.5 ml-2' price={price * qty} />
            </div>
          </div>
          <div className='flex flex-1 items-end justify-between text-sm'>
            <p className='text-gray-500 dark:text-slate-400 flex items-center'>
              <span className='hidden sm:inline-block'>Qty</span>
              <span className='inline-block sm:hidden'>x</span>
              <span className='ml-2'>{qty}</span>
            </p>

            <div className='flex'>
              <button
                type='button'
                className='font-medium text-indigo-600 dark:text-primary-500 '
              >
                Leave review
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const renderOrder = (props: any) => {
    console.log('dfdf', props)
    return (
      <div className='border border-slate-200 dark:border-slate-700 rounded-lg overflow-hidden z-0'>
        <div className='flex flex-col sm:flex-row sm:justify-between sm:items-center p-4 sm:p-8 bg-slate-50 dark:bg-slate-500/5'>
          <div>
            <p className='text-lg font-semibold dark:text-slate-400'>
              #{props._id}
            </p>
            <p className='text-slate-500 dark:text-slate-400 text-sm mt-1.5 sm:mt-2'>
              <span>{new Date(props.createdAt).toLocaleDateString()}</span>
              <span className='mx-2'>·</span>
              <span
                className={
                  props.isDelivered ? 'text-green-500' : 'text-red-500'
                }
              >
                {props.isDelivered ? 'Enviado' : 'Pendiente'}
              </span>
            </p>
          </div>
          <div className='mt-3 sm:mt-0'>
            <ButtonSecondary
              sizeClass='py-2.5 px-4 sm:px-6'
              fontSize='text-sm font-medium'
            >
              View Order
            </ButtonSecondary>
          </div>
        </div>
        <div className='border-t border-slate-200 dark:border-slate-700 p-2 sm:p-8 divide-y divide-y-slate-200 dark:divide-slate-700'>
          {props?.orderItems?.map((order: any) =>
            renderProductItem(order?.product, order?.qty)
          )}
        </div>
      </div>
    )
  }

  return (
    <div>
      <CommonLayout>
        <div className='space-y-10 sm:space-y-12'>
          {/* HEADING */}
          <h2 className='text-2xl sm:text-3xl font-semibold dark:text-slate-300'>
            Historial de ordenes
          </h2>
          {authOrders.length ? (
            authOrders.map((order) => renderOrder(order))
          ) : (
            <Message
              message='No has hecho tu primera compra. Busca tu producto deasado y ordenalo'
              type='error'
            />
          )}
        </div>
      </CommonLayout>
    </div>
  )
}

export default AccountOrder
