import { ButtonPrimary } from '@/components/shared/Button'
import CommonLayout from './components/CommonLayout'
import Label from '@/components/Label'
import Input from '@/components/shared/Inputs/Input'

const AccountBilling = () => {
  return (
    <div>
      <CommonLayout>
        <div className='space-y-10 sm:space-y-12'>
          {/* HEADING */}
          <h2 className='text-2xl sm:text-3xl font-semibold dark:text-slate-300'>
            Datos de envio
          </h2>
          <div className=' max-w-xl space-y-6'>
            <div>
              <Label>Provincia</Label>
              <Input type='password' className='mt-1.5' />
            </div>
            <div>
              <Label>Numero</Label>
              <Input type='password' className='mt-1.5' />
            </div>
            <div className='pt-2'>
              <ButtonPrimary>Actualizar Datos de envio</ButtonPrimary>
            </div>
          </div>
        </div>
      </CommonLayout>
    </div>
  )
}

export default AccountBilling
