import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import Checkbox from '@/components/shared/Inputs/CheckBox'
import Radio from '@/components/shared/Inputs/Radio'
import { useCategoryBrandState } from '@/context/categoryBrand'
import { sizes } from '@/data/data'
import Slider from 'rc-slider'
import { useProductsState } from '../context'

// DEMO DATA

const DATA_sortOrderRadios = [
  { name: 'Most Popular', id: 'Most-Popular' },
  { name: 'Best Rating', id: 'Best-Rating' },
  { name: 'Newest', id: 'Newest' },
  { name: 'Price Low - Hight', id: 'Price-low-hight' },
  { name: 'Price Hight - Low', id: 'Price-hight-low' },
]

const PRICE_RANGE = [1, 500]
//
const SidebarFilters = () => {
  const { category } = useParams()

  const { getProducts } = useProductsState()
  const { brands } = useCategoryBrandState()
  //
  const [rangePrices, setRangePrices] = useState([100, 500])
  const [selectedBrands, setSelectedBrands] = useState<any[]>([])
  const [sizesState, setSizesState] = useState<string[]>([])
  const [sortOrderStates, setSortOrderStates] = useState<string>('')
  const [priceSort, setPriceSort] = useState('')

  //
  const handleChangeBrands = (checked: boolean, id: string) => {
    checked
      ? setSelectedBrands([...selectedBrands, id])
      : setSelectedBrands(selectedBrands.filter((i) => i !== id))
  }

  const handleChangeSizes = (checked: boolean, name: string) => {
    checked
      ? setSizesState([...sizesState, name])
      : setSizesState(sizesState.filter((i) => i !== name))
  }
  //

  useEffect(() => {
    let query: any = { category }

    if (selectedBrands.length) query.brand = selectedBrands.join(',')
    if (sizesState.length) query.sizes = sizesState.join(',')
    if (priceSort) query.price = priceSort

    handleGetProducts(query)
  }, [selectedBrands, sizesState, priceSort])

  const handleGetProducts: any = async (query: any) => {
    try {
      await getProducts(query)
    } catch (error: any) {
      console.log('FETCH FROM FILTERS:', error.message)
    }
  }

  // OK
  const renderTabsSize = () => {
    return (
      <div className='relative flex flex-col py-8 space-y-4'>
        <h3 className='font-semibold mb-2.5'>brands</h3>
        {sizes.map((item) => (
          <div key={item.name} className='uppercase'>
            <Checkbox
              name={item.name}
              label={item.name}
              defaultChecked={sizesState.includes(item.name)}
              onChange={(checked) => handleChangeSizes(checked, item.name)}
              sizeClassName='w-5 h-5'
              labelClassName='text-sm font-normal'
            />
          </div>
        ))}
      </div>
    )
  }

  // OK
  const renderTabsBrand = () => {
    return (
      <div className='relative flex flex-col py-8 space-y-4'>
        <h3 className='font-semibold mb-2.5'>Sizes</h3>
        {brands.map((item) => (
          <div key={item.name} className='uppercase'>
            <Checkbox
              name={item.name}
              label={item.name}
              defaultChecked={sizesState.includes(item.name)}
              onChange={(checked) => handleChangeBrands(checked, item._id)}
              sizeClassName='w-5 h-5'
              labelClassName='text-sm font-normal'
            />
          </div>
        ))}
      </div>
    )
  }

  // OK
  const renderTabsPriceRage = () => {
    return (
      <div className='relative flex flex-col py-8 space-y-5 pr-3'>
        <div className='space-y-5'>
          <span className='font-semibold'>Price range</span>
          <Slider
            range
            min={PRICE_RANGE[0]}
            max={PRICE_RANGE[1]}
            step={1}
            defaultValue={[rangePrices[0], rangePrices[1]]}
            allowCross={false}
            onChange={(_input: number | number[]) =>
              setRangePrices(_input as number[])
            }
          />
        </div>

        <div>
          <Radio
            name='desc'
            id='1'
            label='Mayor a menor'
            className='mb-4'
            onChange={() => setPriceSort('desc')}
          />
          <Radio
            name='desc'
            id='1'
            label='Menor a mayor'
            onChange={() => setPriceSort('asc')}
          />
        </div>
      </div>
    )
  }

  // OK
  const renderTabsSortOrder = () => {
    return (
      <div className='relative flex flex-col py-8 space-y-4'>
        <h3 className='font-semibold mb-2.5'>Sort order</h3>
        {DATA_sortOrderRadios.map((item) => (
          <Radio
            id={item.id}
            key={item.id}
            name='radioNameSort'
            label={item.name}
            defaultChecked={sortOrderStates === item.id}
            sizeClassName='w-5 h-5'
            onChange={setSortOrderStates}
            className='!text-sm'
          />
        ))}
      </div>
    )
  }

  return (
    <div className='divide-y divide-slate-200 dark:divide-slate-700 dark:text-neutral-300'>
      {renderTabsBrand()}
      {renderTabsSize()}
      {renderTabsPriceRage()}

      {renderTabsSortOrder()}
    </div>
  )
}

export default SidebarFilters
