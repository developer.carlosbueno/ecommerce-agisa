/* eslint-disable no-empty */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Product } from '@/models/Product'
import React, {
  ReactElement,
  createContext,
  useContext,
  useEffect,
  useState,
} from 'react'
import { getproducts as fetchProducts, getproduct } from '../services'

// Services

const initialState: InitialStateProps = {
  products: [],
  searchProducts: [],
  product: null,
  getProducts: async () => {},
  getProduct: async () => {},
  setSearch: () => {},
  setProduct: () => {},
}

const ProductsContext = createContext(initialState)

export interface ProductsProviderProps {
  children: ReactElement
}

export const ProductsProvider: React.FC<ProductsProviderProps> = ({
  children,
}) => {
  const [products, setProducts] = useState<Product[]>([])
  const [searchProducts, setSearchProducts] = useState<Product[]>([])
  const [search, setSearch] = useState<string>('')
  const [product, setProduct] = useState<Product | null>(initialState.product)

  const activeProducts = search ? searchProducts : products

  useEffect(() => {
    getInitialState()
  }, [])

  useEffect(() => {
    console.log('PRODUCT CHANGE', products)
  }, [products])

  useEffect(() => {
    if (search) {
      handleSearchProduct()
    }
  }, [search])

  const handleSearchProduct = () => {
    const searched = products.filter((p) =>
      p.name.toLowerCase().includes(search.toLowerCase())
    )

    setSearchProducts(searched)
  }

  const getProducts = async (query: any) => {
    try {
      const productsData = await fetchProducts(query)

      setProducts(productsData)
    } catch (error) {}
  }

  const getProduct = async (id: string) => {
    try {
      const productsData = await getproduct(id)

      setProduct(productsData)
    } catch (error) {}
  }

  const getInitialState = async () => {
    try {
      const productsData = await fetchProducts()
      setProducts(productsData)
    } catch (error) {}
  }

  return (
    <ProductsContext.Provider
      value={{
        product,
        products: activeProducts,
        getProduct,
        getProducts,
        setProduct,
        searchProducts,
        setSearch,
      }}
    >
      {children}
    </ProductsContext.Provider>
  )
}

export const useProductsState = () => useContext(ProductsContext)

interface InitialStateProps {
  products: Product[]
  searchProducts: Product[]
  product: Product | null
  getProducts: (queries: any) => Promise<void>
  getProduct: (id: string) => Promise<void>
  setSearch: (string: string) => void
  setProduct: (product: Product) => void
}
