import { DataService } from "@config/api";
import { Product } from "@/models/Product";

export const getproducts = async (query?: any): Promise<Product[]> => {

  let keys = Object.keys(query)
  let queryString: string = ''

  if (keys.length) {
    keys?.map((item, i) => queryString += `${item}=${query[keys[i]]}&`)
  }

  try {
    const { data } = await DataService.get(`/products?categories=${query.category}&` + queryString)
    return data
  } catch (error: any) {
    throw new Error(error.data.response.error || error.message)
  }
}


export const getproduct = async (id: string): Promise<Product> => {

  try {
    const { data } = await DataService.get(`/products/${id}`)
    return data
  } catch (error: any) {
    throw new Error(error.data.response.error || error.message)
  }
}
