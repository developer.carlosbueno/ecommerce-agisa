import { FC, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { ProductCard } from '@/components'
import { Helmet } from 'react-helmet-async'
import SectionSliderCollections from './components/SectionSliderLargeProduct'
import SidebarFilters from './components/SidebarFilters'
import { useProductsState } from './context'
import { useCategoryBrandState } from '@/context/categoryBrand'

export interface ProductsProps {
  className?: string
}

const Products: FC<ProductsProps> = ({ className = '' }) => {
  const { getProducts } = useProductsState()

  const { category } = useParams()
  const { products } = useProductsState()
  const { categories } = useCategoryBrandState()

  useEffect(() => {
    handleGetProducts()
  }, [category])

  const handleGetProducts = async () => {
    await getProducts({ category })
    try {
    } catch (error: any) {
      console.log('ERROR FETCHING ORDERS FROM PRODUCTS: ', error.message)
    }
  }

  return (
    <div
      className={`nc-PageCollection2 overflow-hidden ${className}`}
      data-nc-id='PageCollection2'
    >
      <Helmet>
        <title>Category || Ciseco Ecommerce Template</title>
      </Helmet>

      <div className='container py-16 lg:pb-28 lg:pt-20 space-y-16 sm:space-y-20 lg:space-y-28'>
        <div className='space-y-10 lg:space-y-14'>
          {/* HEADING */}
          <div className='max-w-screen-sm'>
            <h2 className='block capitalize text-2xl sm:text-3xl lg:text-4xl font-semibold dark:text-neutral-300'>
              {categories.find((i) => i._id === category)?.name}
            </h2>
          </div>

          <hr className='border-slate-200 dark:border-slate-700' />
          <main>
            {/* LOOP ITEMS */}
            <div className='flex flex-col lg:flex-row'>
              <div className='lg:w-1/3 xl:w-1/4 pr-4'>
                <SidebarFilters />
              </div>
              <div className='flex-shrink-0 mb-10 lg:mb-0 lg:mx-4 border-t lg:border-t-0'></div>
              <div className='flex-1 '>
                <div className='flex-1 grid sm:grid-cols-2 xl:grid-cols-3 gap-x-8 gap-y-10 '>
                  {products?.map((item, index) => (
                    <ProductCard data={item} key={index} />
                  ))}
                </div>
              </div>
            </div>
          </main>
        </div>

        {/* === SECTION 5 === */}
        <hr className='border-slate-200 dark:border-slate-700' />

        <SectionSliderCollections />
        <hr className='border-slate-200 dark:border-slate-700' />

        {/* SUBCRIBES */}
      </div>
    </div>
  )
}

export default Products
