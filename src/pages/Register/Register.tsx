import { FC, useEffect, useState } from 'react'
import { Helmet } from 'react-helmet-async'
import Input from '@/components/shared/Inputs/Input'
import { Link, useNavigate } from 'react-router-dom'
import { ButtonPrimary } from '@/components/shared/Button'
import { getLocalStore } from '@/utils/getLocalStore'
import { useAuthState } from '@/context/auth.context'
import toast from 'react-hot-toast'

export interface PageRegisterProps {
  className?: string
}

const registerSocials = [
  {
    name: 'Continue with Facebook',
    href: '#',
    icon: 'https://ciseco-reactjs.vercel.app/static/media/facebook.8291c7f7c187e8f09292cced2ed0278d.svg',
  },
  {
    name: 'Continue with Twitter',
    href: '#',
    icon: 'https://ciseco-reactjs.vercel.app/static/media/twitter.f56ce1bc9eb5120250ac80ed561cf82f.svg',
  },
  {
    name: 'Continue with Google',
    href: '#',
    icon: 'https://ciseco-reactjs.vercel.app/static/media/Google.b9361a382296ba2cbc182016085b0cc8.svg',
  },
]

const PageRegister: FC<PageRegisterProps> = ({ className = '' }) => {
  const navigate = useNavigate()
  const { register, auth } = useAuthState()

  const [fullname, setFullname] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')

  useEffect(() => {
    if (auth) {
      navigate('/')
    }
  }, [auth])

  const handleRegisterUser = async () => {
    try {
      if (!email || !fullname || !password || !repeatPassword) {
        toast.error('Llena todos los campos!')
        return
      }

      if (password !== repeatPassword) {
        toast.error('Las contraseñas no coinciden')
        return
      }

      await register({ fullname, email, password })
      toast.success('Te has registrado exitosamente!')
    } catch (error: any) {
      toast.error(error.message)
      console.log('error registering user', error.message)
    }
  }

  return (
    <div className={`nc-PageLogin ${className}`} data-nc-id='PageLogin'>
      <Helmet>
        <title>Login || Ciseco React Template</title>
      </Helmet>
      <div className='container mb-24 lg:mb-32'>
        <h2 className='my-20 flex items-center text-3xl leading-[115%] md:text-5xl md:leading-[115%] font-semibold text-neutral-900 dark:text-neutral-100 justify-center'>
          Sign up
        </h2>
        <div className='max-w-md mx-auto space-y-6'>
          <div className='grid gap-3'>
            {registerSocials.map((item, index) => (
              <a
                key={index}
                href={item.href}
                className='flex w-full rounded-lg bg-primary-50 dark:bg-neutral-800 px-4 py-3 transform transition-transform sm:px-6 hover:translate-y-[-2px]'
              >
                <img
                  className='flex-shrink-0 w-5'
                  src={item.icon}
                  alt={item.name}
                />
                <h3 className='flex-grow text-center text-sm font-medium text-neutral-700 dark:text-neutral-300 sm:text-sm'>
                  {item.name}
                </h3>
              </a>
            ))}
          </div>
          {/* OR */}
          <div className='relative text-center'>
            <span className='relative z-10 inline-block px-4 font-medium text-sm bg-white dark:text-neutral-400 dark:bg-neutral-900'>
              OR
            </span>
            <div className='absolute left-0 w-full top-1/2 transform -translate-y-1/2 border border-neutral-100 dark:border-neutral-800'></div>
          </div>
          {/* FORM */}
          <form className='grid grid-cols-1 gap-6' action='#' method='post'>
            <label className='block'>
              <span className='text-neutral-800 dark:text-neutral-200'>
                Fullname
              </span>
              <Input
                placeholder='Carlos Bueno'
                value={fullname}
                onChange={(e) => setFullname(e.target.value)}
                className='mt-1'
              />
            </label>
            <label className='block'>
              <span className='text-neutral-800 dark:text-neutral-200'>
                Email address
              </span>
              <Input
                type='email'
                placeholder='example@example.com'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className='mt-1'
              />
            </label>
            <label className='block'>
              <span className='flex justify-between items-center text-neutral-800 dark:text-neutral-200'>
                Password
              </span>
              <Input
                type='password'
                className='mt-1'
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </label>
            <label className='block'>
              <span className='flex justify-between items-center text-neutral-800 dark:text-neutral-200'>
                Repeat Password
              </span>
              <Input
                type='password'
                className='mt-1'
                value={repeatPassword}
                onChange={(e) => setRepeatPassword(e.target.value)}
              />
            </label>
            <ButtonPrimary type='button' onClick={handleRegisterUser}>
              Sign Up
            </ButtonPrimary>
          </form>

          {/* ==== */}
          <span className='block text-center text-neutral-700 dark:text-neutral-300'>
            New user? {` `}
            <Link className='text-green-600' to='/signup'>
              Create an account
            </Link>
          </span>
        </div>
      </div>
    </div>
  )
}

export default PageRegister
